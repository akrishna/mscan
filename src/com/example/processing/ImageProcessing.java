package com.example.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import com.example.camera.CameraActivity;
import com.example.main.CustomToast;
import com.example.main.MainActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class ImageProcessing extends AsyncTask<String, String, String> {
	 
	private Activity myActivity;
	public Mat imageMat;
	private String currentFileName="";
	private CustomToast ct;
	private Object myObject;
	private int img_height;
	private int img_width;
	private String currentSlipId;
	private int outputImageMaxWidth;
	private int outputImageMaxHeight;
	private boolean maxLimitSet=false;
	private boolean isError=false;
	private String error;
	
	public static void rotateImage(String filePath,int rotationAngle) {
		
		Mat scaledImage = Highgui.imread(filePath);
		double radians = Math.toRadians(rotationAngle);
		double sin = Math.abs(Math.sin(radians));
		double cos = Math.abs(Math.cos(radians));

		int newWidth = (int) (scaledImage.width() * cos + scaledImage.height() * sin);
		int newHeight = (int) (scaledImage.width() * sin + scaledImage.height() * cos);

		int[] newWidthHeight = {newWidth, newHeight};

		org.opencv.core.Point centerOld = new org.opencv.core.Point(scaledImage.width()/2, scaledImage.height()/2);
		Size sourceSize = new Size(scaledImage.width(),scaledImage.height());
		Size resultSize=new Size(Math.max(newWidthHeight[0],scaledImage.width()),Math.max(newWidthHeight[1],scaledImage.height()));
		Mat resultMat = new Mat(resultSize,scaledImage.type());
		org.opencv.core.Point centerResult = new org.opencv.core.Point(resultSize.width/2,resultSize.height/2);
		Rect roi=new Rect(new Point(resultSize.width/2-centerOld.x,resultSize.height/2-centerOld.y),sourceSize);
		Mat rotImage = Imgproc.getRotationMatrix2D(centerResult,rotationAngle, 1.0);
		scaledImage.copyTo(resultMat.submat(roi));
		Imgproc.warpAffine(resultMat, resultMat, rotImage, resultSize);
		Highgui.imwrite(filePath,resultMat);
	}
	public ImageProcessing(Activity activity,String fileName,String slipId,Object myObject,int maxWidth,int maxHeight) {
		
		maxLimitSet=true;
		myActivity=activity;
		this.myObject=myObject;
		ct=new CustomToast();
		currentSlipId=slipId;
		currentFileName=fileName;
		imageMat=Highgui.imread(currentFileName);
		outputImageMaxHeight=maxHeight;
	    outputImageMaxWidth=maxWidth;
		
	}
	
   public ImageProcessing(Activity activity,String fileName) {
		
		myActivity= activity;
		ct=new CustomToast();
		currentFileName=fileName;
		imageMat=Highgui.imread(currentFileName);
	  //img_height=imageMat.height();
	  //img_width=imageMat.width();
		
	}
	public ImageProcessing(Activity activity,Mat mat) {
		ct=new CustomToast();
		myActivity=(CameraActivity) activity;
		imageMat=mat;
		
	}
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      
    }


    protected String doInBackground(String... args) {
    	
	       //	ct.showToast(CameraActivity.this,img_height+" "+img_width);
	      	Mat result=new Mat();
	      			result=findLargestRectangle(imageMat);
	       	if(result!=null) {
	       		Highgui.imwrite(currentFileName,result);
	       		//ct.showToast(CameraActivity.this,"saved"+" :"+default_folder_location+currentMenuId+"/"+currentPage+".jpg");
	       	}	
	       	//no need to cover this case
	       /*	
	         else {
	       		
	       		//Highgui.imwrite(currentFileName,tmp);
	       	}*/
	        
	        return null;
    }	

    protected void onPostExecute(String file_url) {
        // dismiss the dialog after getting all products
        if(isError)
        	ct.showToast(myActivity,error);

        if(myObject!=null && myObject.getClass().toString().substring(myObject.getClass().toString().lastIndexOf('.')+1).equals("CameraActivity")) {
        	   MainActivity.imageData.setMetaData(currentFileName,currentSlipId);
        	   CameraActivity cameraActivity=(CameraActivity) myObject;
        	   cameraActivity.currentPageNumber++;
        	   cameraActivity.handler.postDelayed(cameraActivity.update_details,100);
        	   cameraActivity.pDialog.dismiss();
        }
    
    } 
    
    
    private Mat findLargestRectangle(Mat original_image) {

           List<Point> sourceOrder=getMenuVertices(original_image);
           if(sourceOrder.size()==0) {
        	   
        	   isError=true;
          	   error="results are not better please disable auto processing,original saved";
          	   Log.d("findLargestRectangle","all the corners were not detected well");
          	   //ct.showToast(myActivity,"results are not better please disable auto processing,original saved");
          	   return null;
        	   
           }
           img_height=getDistance(sourceOrder.get(0),sourceOrder.get(1));
           img_width=getDistance(sourceOrder.get(1),sourceOrder.get(2));
           
           /*
            * 
            *  when the detected Menu size is beyond the set MaxWidth,MaxHeight parameters
            */
           if(maxLimitSet) {
        	   
        	   if(img_width>outputImageMaxWidth && img_height>outputImageMaxHeight) {
        		   
        		   img_height=(outputImageMaxWidth*img_height)/img_width;
        		   img_width=outputImageMaxWidth;
        	   }
        	   else if(img_width>outputImageMaxWidth) {
        		   
        		   img_height=(outputImageMaxWidth*img_height)/img_width;
        		   img_width=outputImageMaxWidth;
        		   
        	   }
        	   else if(img_height>outputImageMaxHeight) {
        		   
        		   img_width=(outputImageMaxHeight*img_width)/img_height;
        		   img_height=outputImageMaxHeight;
        	   }
           }
           try {
	           Core.circle(original_image,sourceOrder.get(0),100,new Scalar(0,0,255));
	           Core.circle(original_image,sourceOrder.get(1),130,new Scalar(255,0,0));
	           Core.circle(original_image,sourceOrder.get(2),130,new Scalar(0,255,0));
	           Core.circle(original_image,sourceOrder.get(3),100,new Scalar(0,255,255));
           }
           catch(IndexOutOfBoundsException ex) {
           	
        	   	isError=true;
          	 	error="something went wrong in getVertices order"+ex.getMessage();
           	    //ct.showToast(myActivity,"something went wrong in getVertices order"+top.size()+" "+bot.size());
           }
           Mat startM = Converters.vector_Point2f_to_Mat(sourceOrder);
           Mat result=warp(Highgui.imread(currentFileName),startM);
           return result;
    }

    public List<Point> getVerticesInOrder(List<Point> source) {
     
     int centerX=0,centerY=0;
     List<Point> sourceOrder=new ArrayList<Point>();
     Vector<Point> top=new Vector<Point>(); 
     Vector<Point> bot=new Vector<Point>();
     for(int i=0;i<source.size();i++) {
    	 
    	 centerX+=source.get(i).x;
    	 centerY+=source.get(i).y;
     }
     
     centerX=centerX/source.size();
     centerY=centerY/source.size();
     
     for(int i=0;i<source.size();i++) {
    	 
    	 if(source.get(i).y<=centerY && top.size()<2) {
    		 
    		 top.addElement(source.get(i));
    	 }
    	 else{
    		 
    		 bot.addElement(source.get(i));
    	 }
     }
        try {
        	Log.d("getVerticesInOrder",top.size()+"=>"+bot.size());
    	 	Point tl = top.get(0).x > top.get(1).x ? top.get(1) : top.get(0);
    	    Point tr = top.get(0).x > top.get(1).x ? top.get(0) : top.get(1);
    	    Point bl = bot.get(0).x > bot.get(1).x ? bot.get(1) : bot.get(0);
    	    Point br = bot.get(0).x > bot.get(1).x ? bot.get(0) : bot.get(1);
    	    
    	    sourceOrder.add(bl);
    		sourceOrder.add(br);
    		sourceOrder.add(tr);
    		sourceOrder.add(tl);
        }
        catch(IndexOutOfBoundsException ex) {
        	
        	isError=true;
       	 	error="something went wrong in getVertices order"+top.size()+" "+bot.size();
       	 	Log.d("getVerticesInOrder",ex.getMessage());
        	//ct.showToast(myActivity,"something went wrong in getVertices order"+top.size()+" "+bot.size());
        }
    	
     
     return sourceOrder;
    }

    public Mat warp(Mat inputMat,Mat startM) {

        Mat outputMat = new Mat(img_width, img_height, CvType.CV_8UC4);
        //ct.showToast(myActivity,"output resolution"+img_height+"->"+img_width);
        Point ocvPOut1 = new Point(0, 0);
        Point ocvPOut2 = new Point(0,img_height);
        Point ocvPOut3 = new Point(img_width, img_height);
        Point ocvPOut4 = new Point(img_width, 0);
        List<Point> dest = new ArrayList<Point>();
        dest.add(ocvPOut1);
        dest.add(ocvPOut2);
        dest.add(ocvPOut3);
        dest.add(ocvPOut4);
        Mat endM = Converters.vector_Point2f_to_Mat(dest);      
        try {
        
        	Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);
        
        	Imgproc.warpPerspective(inputMat, 
                outputMat,
                perspectiveTransform,
                new Size(img_width, img_height), 
                Imgproc.INTER_CUBIC);
        }
        catch(CvException cv) {
        	Log.d("error","image not generated or not found"+cv.getMessage());
        	isError=true;
       	 	error="image not generated or not found "+cv.getMessage();
        	//	ct.showToast(myActivity,"image not generated or not found"+cv.getMessage());
        	return null;
       	 
       }
        return outputMat;
    }
    
    public int getDistance(Point p1,Point p2) {
    	
    	return (int) Math.sqrt(Math.pow(p1.x-p2.x, 2)+Math.pow(p1.y-p2.y, 2));
    }
    
    public  List<Point> getMenuVertices(Mat original_image) {
        Mat imgSource = new Mat();
        imgSource=original_image;
        List<Point> cornerPoints=new ArrayList<Point>();
        
        //  Mat sourceImage;
        //convert the image to black and white
        Imgproc.cvtColor(imgSource, imgSource, Imgproc.COLOR_BGR2GRAY);

        //convert the image to black and white does (8 bit)
        Imgproc.Canny(imgSource, imgSource, 30, 40);

        //apply gaussian blur to smoothen lines of dots
        Imgproc.GaussianBlur(imgSource, imgSource, new  org.opencv.core.Size(5, 5), 5);

        //find the contours
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        try {
        	Imgproc.findContours(imgSource, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
            Highgui.imwrite(Environment.getExternalStorageDirectory().
     	        	 getAbsolutePath() +"/scan/L/0.jpg",imgSource);
        }
        catch(CvException cv) {
        	 Log.d("error","image not generated or not found"+cv.getMessage());
        	 isError=true;
        	 error="image not generated or not found"+cv.getMessage();
        	// ct.showToast(myActivity,"image not generated or not found"+cv.getMessage());
        	 return null;
        	 
        }
        if(contours.size()!=0) {
            double maxArea = -1;
            Log.d("size",Integer.toString(contours.size()));
            MatOfPoint temp_contour = contours.get(0); //the largest is at the index 0 for starting point
            MatOfPoint2f approxCurve = new MatOfPoint2f();
            MatOfPoint largest_contour = contours.get(0);
            int maxAreaIdx=0;
            List<MatOfPoint> largest_contours = new ArrayList<MatOfPoint>();
            for (int idx = 0; idx < contours.size(); idx++) {
                temp_contour = contours.get(idx);
                double contourarea = Imgproc.contourArea(temp_contour);
                //compare this contour to the previous largest contour found
                if (contourarea > maxArea) {
                    //check if this contour is a square
                    MatOfPoint2f new_mat = new MatOfPoint2f( temp_contour.toArray() );
                    int contourSize = (int)temp_contour.total();
        	        MatOfPoint2f approxCurve_temp = new MatOfPoint2f();
                    Imgproc.approxPolyDP(new_mat, approxCurve_temp, contourSize*0.05, true);
                    if (approxCurve_temp.total() == 4) {
                        maxArea = contourarea;
                        approxCurve=approxCurve_temp;
                        largest_contour = temp_contour;
                        maxArea = contourarea;
                        maxAreaIdx = idx;
                        approxCurve=approxCurve_temp;
                    }
                }
            }
           largest_contours.add(largest_contour);

           Imgproc.cvtColor(imgSource, imgSource, Imgproc.COLOR_BayerBG2RGB);
           Imgproc.drawContours(imgSource,contours,maxAreaIdx,new Scalar(0,0,255));
           //Utils.bitmapToMat(bmp,sourceImage);
           double[] temp_double;
           temp_double = approxCurve.get(0,0);       
           Point p1 = new Point(temp_double[0], temp_double[1]);
         //  Core.circle(imgSource,p1,50,new Scalar(255,255,255));
           
           temp_double = approxCurve.get(1,0);       
           Point p2 = new Point(temp_double[0], temp_double[1]);
          // Core.circle(imgSource,p2,100,new Scalar(255,255,255));
           temp_double = approxCurve.get(2,0);     
           Point p3 = new Point(temp_double[0], temp_double[1]);
         //  Core.circle(imgSource,p3,150,new Scalar(255,255,255));
           temp_double = approxCurve.get(3,0);       
           Point p4 = new Point(temp_double[0], temp_double[1]);
         //  Core.circle(imgSource,p4,200,new Scalar(255,255,255));

           List<Point> source = new ArrayList<Point>();
           source.add(p1);
           source.add(p2);
           source.add(p3);
           source.add(p4);
           
           cornerPoints=getVerticesInOrder(source);
           Mat result=Highgui.imread(currentFileName);
           
          // Core.circle(result,cornerPoints.get(0),100,new Scalar(0,0,255));
          // Core.circle(result,cornerPoints.get(1),130,new Scalar(255,0,0));
         //  Core.circle(result,cornerPoints.get(2),130,new Scalar(0,255,0));
         //  Core.circle(result,cornerPoints.get(3),100,new Scalar(0,255,255));
           Highgui.imwrite(currentFileName,result);
        }
        return cornerPoints;
   }
    
    public List<android.graphics.Point> getAndroidPoints(List<org.opencv.core.Point> openCVPoint) {
    	
    	List<android.graphics.Point> androidPoint=new ArrayList<android.graphics.Point>();
    	for(int i=0;i<openCVPoint.size();i++) {
    		androidPoint.add(new android.graphics.Point((int)openCVPoint.get(i).x,(int)openCVPoint.get(i).y));
    	}
    	
    	return androidPoint;
    }

}
