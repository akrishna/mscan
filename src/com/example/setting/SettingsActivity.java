package com.example.setting;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.example.main.MainActivity;
import com.example.nevigation.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Bundle;

import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;



/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		 addPreferencesFromResource(R.layout.preference);
		 setContentView(R.layout.preferences_layout);
		 Button allDelete=(Button) findViewById(R.id.allDelete);
		 allDelete.setOnClickListener(new Button.OnClickListener() {
			    public void onClick(View v) {

			    	 AlertDialog.Builder all_delete_alert=new AlertDialog.Builder(SettingsActivity.this);
				     all_delete_alert.setTitle("confirm all delete ?");
				     all_delete_alert.setPositiveButton("OK",
			                    new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog,int which) {
		                   	
		                      File directory=new File(MainActivity.default_folder_location);
		                      try {
					            	FileUtils.deleteDirectory(directory);
					            }
					            catch(IOException ex)  {
					            	
					            	Log.d("SettingsActivity",ex.getMessage());
					            }

		                   }
		                   
		                   
				     });
				     all_delete_alert.setNegativeButton("Cancel",
			                    new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog,int which) {
		                   }
				     });
				     
				     all_delete_alert.show();
				
			    }
			});
		 Camera camera=Camera.open();
		 List<Size> sizeList=camera.getParameters().getSupportedPictureSizes();
		 camera.release();
		 CharSequence[] entries=new CharSequence[sizeList.size()];
		 for(int i=0;i<sizeList.size();i++) {
			 entries[i]=sizeList.get(i).width+"*"+sizeList.get(i).height;
		 }
		 @SuppressWarnings("deprecation")
		 ListPreference lp = (ListPreference)findPreference("prefInputImage");
		 lp.setEntries(entries);
		 lp.setEntryValues(entries);
		 
	}


}
