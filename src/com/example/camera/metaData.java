package com.example.camera;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.media.ExifInterface;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

public class metaData {
	
	private double latitude;
	private double longitude;
	private ExifInterface exif;
	private String dateTime;
	private GPSTracker gps;
	private String IMEI;
	private String android_id;
	private Activity activity;
	private String filePath;
	public metaData(Activity activity) {
		
		gps=new GPSTracker(activity);
		this.activity=activity;
		if(gps.canGetLocation()){

				latitude = gps.getLatitude();
				longitude = gps.getLongitude();
				
	    }else{
				gps.showSettingsAlert();
		}
		
		SimpleDateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
		dateTime = df.format(Calendar.getInstance().getTime());
		
		TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
		IMEI=telephonyManager.getDeviceId();
		
		android_id = Secure.getString(activity.getContentResolver(),
                Secure.ANDROID_ID); 
		
	}
	public void setMetaData(String filePath,String slipId) {
		
		this.filePath=filePath;
		try {
			exif = new ExifInterface(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("lat",latitude+"");
		Log.d("long",longitude+"");
		Log.d("date/time",dateTime);
		Log.d("IMEI",IMEI);
		Log.d("Android_id",android_id);
		Log.d("GPS Accuracy",gps.getLocation().getAccuracy()+"");
		Log.d("Processing method",gps.getLocation().getProvider()+"");
		Log.d("latitude",gps.getLocation().getAltitude()+"");
		exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE,Double.toString(latitude));
		exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,Double.toString(longitude));
		exif.setAttribute(ExifInterface.TAG_DATETIME,dateTime);
		exif.setAttribute(ExifInterface.TAG_MODEL,IMEI);
		exif.setAttribute(ExifInterface.TAG_MAKE,slipId+"#"+gps.getLocation().getAccuracy());
		exif.setAttribute(ExifInterface.TAG_GPS_PROCESSING_METHOD,gps.getLocation().getProvider());
		exif.setAttribute(ExifInterface.TAG_GPS_ALTITUDE,Double.toString(gps.getLocation().getAltitude()));
		
		
		try {
			exif.saveAttributes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 try 
         {
                 exif = new ExifInterface(filePath);
                 StringBuilder builder = new StringBuilder();
                 
                 Log.d("result lat",exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE)+ "->"+exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)+"->"+exif.getAttribute(ExifInterface.TAG_DATETIME));
         }
         catch (IOException e) 
         {
                 e.printStackTrace();
         }                        
	}
}
