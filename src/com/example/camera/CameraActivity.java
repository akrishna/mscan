package com.example.camera;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import com.example.nevigation.R;
import com.google.zxing.client.android.CaptureActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.example.main.*;
public class CameraActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "OCVSample::Activity";
    public int currentPageNumber=0;
    private int startingPageNumber=0;
    private String currentSlipId="ungrouped";
    public ProgressDialog pDialog;
    private MyView mOpenCvCameraView;
    private String default_folder_location;
    public static CustomToast ct;
    private Bitmap bmp;
    public Handler handler;
    private String currentFileName="";
    private int outputImageMaxWidth;
    private int outputImageMaxHeight;
    private int inputImageHeight;
    private int inputImageWidth;
    private boolean ifAutoProcessingOn;
    private boolean ifImageCompressionOn;
    private boolean isCameraResolutionFine=true;
/*    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(CameraActivity.this);
                    handler.postDelayed(set_resolution,200);
                   ct.showToast(CameraActivity.this,outputImageWidth+"=="+outputImageHeight+")"+mOpenCvCameraView.getResolution().height+"->"+mOpenCvCameraView.getResolution().width );
                    String str ="";
                    for(int i=0;i<mOpenCvCameraView.getResolutionPictureList().size();i++) {
                    	str=str+mOpenCvCameraView.getResolutionPictureList().get(i).width+" ->"+mOpenCvCameraView.getResolutionPictureList().get(i).height+'\n';
                    }
                    ct.showToast(CameraActivity.this,str );
                 }
                 break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    }; */
        
    public CameraActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        ct=new CustomToast();
        default_folder_location=MainActivity.default_folder_location;
        handler=new Handler();
        currentSlipId=getIntent().getExtras().getString("currentSlipId");
        currentPageNumber=getIntent().getExtras().getInt("currentPageNumber");
        startingPageNumber=currentPageNumber;

       
        //get session variables 
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(CameraActivity.this);
    	ifAutoProcessingOn=sharedPrefs.getBoolean("prefScanMode",false);
    	ifImageCompressionOn=sharedPrefs.getBoolean("prefCompressionMode",false);
    	outputImageMaxWidth=Integer.parseInt(sharedPrefs.getString("prefOutputImageWidth","650"));
    	outputImageMaxHeight=Integer.parseInt(sharedPrefs.getString("prefOutputImageHeight","850"));
    	String strInput=sharedPrefs.getString("prefInputImage","2304*1728");
    	if(strInput==null) {
    		 isCameraResolutionFine = false;
    	}
    	else {
    		inputImageHeight=getHeight(strInput);
            inputImageWidth=getWidth(strInput);
    	}
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_camera);

        mOpenCvCameraView = (MyView) findViewById(R.id.camera_holder);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
       // ct.showToast(CameraActivity.this,mOpenCvCameraView.getResolution().height+"->"+mOpenCvCameraView.getResolution().width );
       // mOpenCvCameraView.setMaxFrameSize(img_height, img_width);
        mOpenCvCameraView.setCvCameraViewListener(this);
       // mOpenCvCameraView.setResolution(Camera.new Size(img_height,img_width));
        Button cancel=(Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		    	Intent mIntent = new Intent();
		 		mIntent.putExtra("numPageScanned",(currentPageNumber-startingPageNumber));
		        setResult(RESULT_OK, mIntent);
		    	finish();
		    }
		});
        
        handler.postDelayed(update_details,1000);
        mOpenCvCameraView.enableView();
        mOpenCvCameraView.setOnTouchListener(CameraActivity.this);
        handler.postDelayed(set_resolution,200);
  
    }

 
	@Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
      //  OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    	
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        return inputFrame.rgba();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG,"onTouch event");
        pDialog=new ProgressDialog(CameraActivity.this);
        pDialog = new ProgressDialog(CameraActivity.this);
        pDialog.setMessage("processing image... ");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        currentFileName =default_folder_location+currentSlipId+"/menu/"+currentPageNumber+".jpg";
        mOpenCvCameraView.takePicture(CameraActivity.this,currentFileName,currentSlipId,outputImageMaxHeight,outputImageMaxWidth,ifAutoProcessingOn);
      //  ct.timeToast(CameraActivity.this,"scanned",200);
     /*   if(ifAutoProcessingOn) {
        	handler.postDelayed(check_if_generated,1500);
        } 	*/
       // currentPage++;
	    
        return false;
    }
    
    public Runnable set_resolution=new Runnable() {
    	
    	public void run() {
    		if(isCameraResolutionFine) {
    			mOpenCvCameraView.setResolution(inputImageWidth,inputImageHeight);
    		}
    		else {
    			
    		}
    	}
    };	
    
 /*  public Runnable check_if_generated=new Runnable() {
    	
    	public void run() {
    		 //   bmp=BitmapFactory.decodeByteArray(MyView.imageData, 0, MyView.imageData.length);
    	     ImageProcessing process=new ImageProcessing(CameraActivity.this,currentFileName,outputImageMaxHeight,outputImageMaxWidth);
    	     process.execute();
   
    	} 	
    };*/
    public int getStartingPage() {
    	
    	return startingPageNumber;
    }
    
    public void setStartingPage(int page) {
    	
    	startingPageNumber=page;
    }
    
    public String getCurrentMenuId() {
    	
    	return currentSlipId;
    }
    
    public void setStartingPage(String menuId) {
    	
    	currentSlipId=menuId;
    }
    
    public Runnable update_details=new Runnable() {
    	
    	public void run() {
	    	TextView folder=(TextView) findViewById(R.id.currentSlipId);
	    	folder.setText(currentSlipId);
	    	TextView numPage=(TextView) findViewById(R.id.numPagesValue);
	    	numPage.setText((currentPageNumber-1)+"");
	    	TextView instruction=(TextView) findViewById(R.id.instruction);
	    	instruction.setText("please scan "+currentPageNumber+"th page");
	    	
    	} 	
    };
    
    @Override
    public void onBackPressed() {
    	Intent mIntent = new Intent();
 		mIntent.putExtra("numPageScanned",(currentPageNumber-startingPageNumber));
        setResult(RESULT_OK, mIntent);
        super.onBackPressed();
    }
    
   
    public int getWidth(String string) {
    	
    	return Integer.parseInt(string.substring(0,string.indexOf('*')));

    }
    
    public int getHeight(String string) {
    	
    	return Integer.parseInt(string.substring(string.indexOf('*')+1));
    }
    
}
