package com.example.camera;

import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.io.*;

import org.opencv.android.JavaCameraView;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;

import com.example.main.MainActivity;
import com.example.processing.*;
public class MyView extends JavaCameraView implements PictureCallback {

    private static final String TAG = "Sample::Tutorial3View";
    static String mPictureFileName;
    static byte[] imageData;
    private boolean ifAutoProcessingOn;
    private CameraActivity cameraActivity;
    private int outputImageMaxHeight,outputImageMaxWidth;
    private String currentSlipId;
    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public List<Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }
    
    public List<Size> getResolutionPictureList() {
        return mCamera.getParameters().getSupportedPictureSizes();
    }
    
    
    public void setMaxResolution() {
        /* disconnectCamera();
         mMaxHeight = resolution.height;
         mMaxWidth = resolution.width;
         connectCamera(getWidth(), getHeight());*/
     	//disconnectCamera();
     	Camera.Parameters params = mCamera.getParameters();
     	List<Size> supportedSizes=params.getSupportedPictureSizes();
     	Collections.sort(supportedSizes,new Comparator<Size>() {
            public int compare(Size o1,Size o2) {
            	Log.d("check",o1.width+"");
                return o1.width<o2.width?1:-1;
            } 
     	});
     	
     	Size maxSize=supportedSizes.get(0);
     	params.setPictureSize(maxSize.width,maxSize.height);
     	mCamera.setParameters(params);

     // mCamera is a Camera object
     }

    public void setResolution(int width,int height) {
       /* disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());*/
    	//disconnectCamera();
    	Camera.Parameters params = mCamera.getParameters();
    	params.setPictureSize(width,height);
    	try {
    		mCamera.setParameters(params);
    	}
    	catch (java.lang.RuntimeException ex) {
    		this.setMaxResolution();
    	}
    //	connectCamera(getWidth(), getHeight());
    	Log.d("msg",mCamera.getParameters().getPictureSize().width+" "+mCamera.getParameters().getPictureSize().height);
    // mCamera is a Camera object
    }

    public Size getResolution() {
        return mCamera.getParameters().getPictureSize();
    }

    public void takePicture(final CameraActivity myActivity,final String fileName,String slipId,int maxHeight,int maxWidth,boolean ifProcessing) {
        Log.i(TAG, "Taking picture");
        MyView.mPictureFileName = fileName;
        cameraActivity=myActivity;
        outputImageMaxHeight=maxHeight;
        outputImageMaxWidth=maxWidth;
        ifAutoProcessingOn=ifProcessing;
        currentSlipId=slipId;
        //  cameraActivity.ct.showToast(cameraActivity,mCamera.getParameters().getFocusMode());
        if(!mCamera.getParameters().getFocusMode().equals("auto")) {
        	//cameraActivity.ct.showToast(cameraActivity,"not auto");
	        mCamera.autoFocus(new AutoFocusCallback(){
	
				@Override
				public void onAutoFocus(boolean arg0, Camera arg1) {
					// TODO Auto-generated method stub
					
				     // CameraActivity.ct.showToast(myActivity, mCamera.getParameters().getFocusMode());
			        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
			        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
			        mCamera.setPreviewCallback(null);
			        // PictureCallback is implemented by the current class
			        mCamera.takePicture(null, null,MyView.this);
			       // Log.d("report1",mOpenCvCameraView.getResolution().toString() );
			       // Log.d("report2",mOpenCvCameraView.getResolutionList().toString() );
					
				}
	        	
	        	
	        }) ;
        }
        else {
        	// cameraActivity.ct.showToast(cameraActivity,"autohere");
        	// CameraActivity.ct.showToast(myActivity, mCamera.getParameters().getFocusMode());
	        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
	        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
	        mCamera.setPreviewCallback(null);
	        // PictureCallback is implemented by the current class
	        mCamera.takePicture(null, null,MyView.this);
	       // Log.d("report1",mOpenCvCameraView.getResolution().toString() );
	       // Log.d("report2",mOpenCvCameraView.getResolutionList().toString() );
        	
        }

        	
   
    }

	@Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        // Write the image in a file (in jpeg format)
        try {
            FileOutputStream fos = new FileOutputStream(mPictureFileName);
            fos.write(data);
            fos.close();

        } catch (java.io.IOException e) {
        	
            Log.d("PictureDemo", "Exception in photoCallback"+e.getMessage());
        }
        
        //set image meta data 

        
        //lat,long,IMEI,android_id
        
        
        if(ifAutoProcessingOn) {
        	
         ImageProcessing process=new ImageProcessing(cameraActivity,mPictureFileName,currentSlipId,cameraActivity,outputImageMaxWidth,outputImageMaxHeight);
   	     process.execute();
        }
        else {
        	
        	cameraActivity.currentPageNumber++;
        	cameraActivity.pDialog.dismiss();
            cameraActivity.handler.postDelayed(cameraActivity.update_details,100);
            MainActivity.imageData.setMetaData(mPictureFileName,currentSlipId);
        }
       
        
   /*     a=5;
        imageData=data.clone();
        Bitmap bmp=BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap bmp2 = bmp.copy(Bitmap.Config.RGB_565, true);
        Mat imgMat=new Mat();
        Utils.bitmapToMat(bmp2,imgMat);
        MyView.imageData=data.clone();*/
       // Highgui.imwrite(mPictureFileName,imgMat);

    }
    
    
}
