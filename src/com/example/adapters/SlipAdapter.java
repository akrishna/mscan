package com.example.adapters;

import java.util.ArrayList;

import com.example.nevigation.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.item.SlipItem;
import com.example.main.*;
/**
 * 
 * @author manish.s
 *
 */
public class SlipAdapter extends ArrayAdapter<SlipItem> {
	 Context context;
	 int layoutResourceId;
	 ArrayList<SlipItem> data = new ArrayList<SlipItem>();
	
	 public SlipAdapter(Context context, int layoutResourceId,
		  ArrayList<SlipItem> data) {
		  super(context, layoutResourceId, data);
		  this.layoutResourceId = layoutResourceId;
		  this.context = context;
		  this.data = data;
	 }
	 
	 public SlipItem getItemAtPosition(int position) {
		 
		  return data.get(position);
	 }
	
	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
		  View row = convertView;
		  RecordHolder holder = null;
		
		  if (row == null) {
			   LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			   row = inflater.inflate(layoutResourceId, parent, false);
			
			   holder = new RecordHolder();
			   holder.txtTitle = (TextView) row.findViewById(R.id.menuName);
			   row.setTag(holder);
		  } else {
			  holder = (RecordHolder) row.getTag();
		  }
		
		  SlipItem item = data.get(position);
		  holder.txtTitle.setText(item.getMenuName()+"("+item.getpageNum()+"+"+item.getPicNum()+")");
		  return row;
	
	 }
	
	 static class RecordHolder {
		 TextView txtTitle;
	
	 }
}