package com.example.adapters;

import java.util.ArrayList;

import com.example.nevigation.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.item.SlipItem;
import com.example.item.SessionItem;
import com.example.main.*;
/**
 * 
 * @author manish.s
 *
 */
public class SessionAdapter extends ArrayAdapter<SessionItem> {
	 Context context;
	 int layoutResourceId;
	 ArrayList<SessionItem> data = new ArrayList<SessionItem>();
	
	 public SessionAdapter(Context context, int layoutResourceId,
		  ArrayList<SessionItem> data) {
		  super(context, layoutResourceId, data);
		  this.layoutResourceId = layoutResourceId;
		  this.context = context;
		  this.data = data;
	 }
	 
	 public SessionItem getItemAtPosition(int position) {
		 
		  return data.get(position);
	 }
	
	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
		  View row = convertView;
		  RecordHolder holder = null;
		
		  if (row == null) {
			   LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			   row = inflater.inflate(layoutResourceId, parent, false);
			
			   holder = new RecordHolder();
			   holder.pageNum = (TextView) row.findViewById(R.id.pageNum);
			   holder.slipId=(TextView) row.findViewById(R.id.slipId);
			   row.setTag(holder);
		  } else {
			  holder = (RecordHolder) row.getTag();
		  }
		
		  SessionItem item = data.get(position);
		  holder.slipId.setText("("+item.getSlipId()+")");
		  if(item.getFolderType().equals("pic")) {
			  
			  row.findViewById(R.id.sessionParent).setBackgroundColor(Color.parseColor("#FFFAF0"));
			  holder.pageNum.setText(item.getpageNum()+" pic");
			  ImageView imgView=(ImageView) row.findViewById(R.id.sessionIcon);
			  imgView.setImageResource(R.drawable.ic_action_camera);
		  }
		  else {
			  holder.pageNum.setText(item.getpageNum()+" pages");
			  ImageView imgView=(ImageView) row.findViewById(R.id.sessionIcon);
			  imgView.setImageResource(R.drawable.ic_action_new_attachment);
		  }
		  
		  return row;
	
	 }
	
	 static class RecordHolder {
		 TextView pageNum;
		 TextView slipId;
	 }
}