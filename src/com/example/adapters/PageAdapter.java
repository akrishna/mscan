package com.example.adapters;

import java.util.ArrayList;

import com.example.nevigation.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.item.PageItem;
import com.example.main.*;

public class PageAdapter extends ArrayAdapter<PageItem> {
	 Context context;
	 int layoutResourceId;
	 ArrayList<PageItem> data = new ArrayList<PageItem>();
	
	 public PageAdapter(Context context, int layoutResourceId,
	   ArrayList<PageItem> data) {
		  super(context, layoutResourceId,data);
		  this.layoutResourceId = layoutResourceId;
		  this.context = context;
		  this.data = data;
	 }
	 
	 public PageItem getItemAtPosition(int position) {
		 
		  return data.get(position);
	 }
	
	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
		  View row = convertView;
		  RecordHolder holder = null;
		
		  if (row == null) {
			   LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			   row = inflater.inflate(layoutResourceId, parent, false);
			
			   holder = new RecordHolder();
			   holder.txtTitle = (TextView) row.findViewById(R.id.pageName);
			   holder.thumb=(ImageView) row.findViewById(R.id.pageThumb);
			   row.setTag(holder);
		  } else {
			   holder = (RecordHolder) row.getTag();
		  }
		
		  PageItem item = data.get(position);
		  holder.txtTitle.setText(item.getPageName().substring(0,item.getPageName().indexOf(".jpg")));
		  holder.thumb.setImageBitmap(item.getThumbnail());
		  return row;
	
	 }
	
	 static class RecordHolder {
		  TextView txtTitle;
		  ImageView thumb;
	 }
}