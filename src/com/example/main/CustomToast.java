package com.example.main;

import android.app.Activity;
import android.os.Handler;
import android.widget.Toast;

public class CustomToast {

	public void showToast(Activity myActivity,String str){
		
		Toast.makeText(myActivity,str
                , Toast.LENGTH_LONG).show(); 
		
		
	}
	public void timeToast(Activity myActivity,String str,int time) {
		
		final Toast toast = Toast.makeText(myActivity, str, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
               @Override
               public void run() {
                   toast.cancel(); 
               }
        },time);
	}
	public void shortToast(Activity myActivity,String str){
		
		Toast.makeText(myActivity,str
                , Toast.LENGTH_SHORT).show(); 
		
		
	}
}
