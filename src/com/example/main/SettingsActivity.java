package com.example.main;


import java.util.List;

import com.example.nevigation.R;

import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;

import android.preference.ListPreference;
import android.preference.PreferenceActivity;



/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		 addPreferencesFromResource(R.layout.preference);
		 Camera camera=Camera.open();
		 List<Size> sizeList=camera.getParameters().getSupportedPictureSizes();
		 camera.release();
		 CharSequence[] entries=new CharSequence[sizeList.size()];
		 for(int i=0;i<sizeList.size();i++) {
			 entries[i]=sizeList.get(i).width+"*"+sizeList.get(i).height;
		 }
		 @SuppressWarnings("deprecation")
		 ListPreference lp = (ListPreference)findPreference("prefInputImage");
		 lp.setEntries(entries);
		 lp.setEntryValues(entries);
		 
	}


}
