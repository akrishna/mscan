package com.example.main;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.example.setting.*;
import com.example.camera.CameraActivity;
import com.example.nevigation.R;
import com.google.zxing.client.android.CaptureActivity;
import com.example.camera.*;

import android.R.color;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Scan extends Fragment {

	private MainActivity parentActivityContext;
	public Handler handler;
	private View myView;
	private AlertDialog adOption;
	private int LIST_PAGE_CODE;
    @Override
    public void onCreate(Bundle savedInstanceState) {
	     // TODO Auto-generated method stub
	      super.onCreate(savedInstanceState);
	      setHasOptionsMenu(true);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		parentActivityContext=MainActivity.myObject;
		handler=new Handler();
		final View rootView = inflater.inflate(R.layout.scan_frag, container, false);
		myView=rootView;
		Button scanSlipId=(Button) rootView.findViewById(R.id.scanSlipId);
		Button scanMenu=(Button) rootView.findViewById(R.id.scanMenu);
		Button submitSlipId=(Button) rootView.findViewById(R.id.submitSlipId);
		Button capturePic=(Button) rootView.findViewById(R.id.capturePic);
		//page.setEnabled(false);
		handler.postDelayed(update_details,100);
		final TableRow menuRow= (TableRow) rootView.findViewById(R.id.slipIdTableRow);
		//final View settingRow=rootView.findViewById(R.id.setting);
		
	/*	settingRow.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		    	settingRow.setBackgroundColor(Color.GREEN);
		    	Intent i = new Intent(parentActivityContext, SettingsActivity.class);
	            startActivityForResult(i,parentActivityContext.SETTING_SET_CODE);
		    }
		});*/
		
		capturePic.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {

				 Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	             File imageFile=new File(MainActivity.default_folder_location+parentActivityContext.currentSlipId+"/pic/"+parentActivityContext.currentPicNumber+".jpg");
	        	 Uri uriSavedImage = Uri.fromFile(imageFile);
	        	 intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
	        	 intent.putExtra("IMAGE_NAME",parentActivityContext.currentPicNumber+".jpg");
	        	 startActivityForResult(intent,parentActivityContext.NORMAL_IMAGE_CAPTURE);
		    }
		});
		menuRow.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		    	final CharSequence[] items={"menu","pic"};
		        AlertDialog.Builder builderOption=new AlertDialog.Builder(parentActivityContext);
		    	builderOption.setTitle(parentActivityContext.currentSlipId);
		    	/*builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {}
				});*/
		    	
		        builderOption.setNegativeButton("Cancel",
	                    new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog,int which) {
                 	adOption.cancel();
                 }
		        });
		     	builderOption.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
					
						menuRow.setBackgroundColor(Color.RED);
	       				Intent intent=new Intent(parentActivityContext,ListPageActivity.class);
	       				intent.putExtra("currentSlipId",(parentActivityContext.currentSlipId));
	       				intent.putExtra("limitedResult",false);
	       				intent.putExtra("folderType",items[which]);
	       				startActivityForResult(intent,LIST_PAGE_CODE);
					}
			   });
		     	
		     	adOption=builderOption.create();
		     	adOption.show();
			}  	
		}); 
		    
		    
	
		submitSlipId.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		         TextView txt=(TextView)rootView.findViewById(R.id.enterSlipId);
		    	 String text=txt.getText().toString();
		    	 if(text.equals("")) {
		    		
		    		 parentActivityContext.ct.showToast(parentActivityContext,"invalid Menu id");
		    	 }
		    	 else {
		    		txt.setText("");
		    		//page.setEnabled(true);
	                File f = new File(MainActivity.default_folder_location+"/"+text);
	                if(f.isDirectory()) {
	                	parentActivityContext.startingPageNumber=new File(MainActivity.default_folder_location+"/"+text+"/menu/").list().length+1;
	                	parentActivityContext.startingPicNumber=new File(MainActivity.default_folder_location+"/"+text+"/pic/").list().length+1;
	                }
	                else {
	   	                new File(MainActivity.default_folder_location+text+"/menu/").mkdirs(); 
	   	                new File(MainActivity.default_folder_location+text+"/pic/").mkdirs(); 
	   	                
	   	                parentActivityContext.startingPageNumber=1;
	   	                parentActivityContext.startingPicNumber=1;
	   	                parentActivityContext.sessionMenuScanned.put(text,0);
	                }    
	                parentActivityContext.currentSlipId=text;
	                parentActivityContext.currentPageNumber=parentActivityContext.startingPageNumber;
	                parentActivityContext.currentPicNumber=parentActivityContext.startingPicNumber;
	                parentActivityContext.sessionMenuScanned.put(parentActivityContext.currentSlipId, 0);
	                handler.postDelayed(update_details,100);
		    		
		    	} 
		    }
		});
		
		scanSlipId.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		    	Intent intent = new Intent(parentActivityContext,CaptureActivity.class);
		    	intent.setAction("com.google.zxing.client.android.SCAN");
		        startActivityForResult(intent,parentActivityContext.DETECT_BAR_CODE_REQUEST_CODE );
		    }
		});

		scanMenu.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
		    	Intent intent = new Intent(parentActivityContext,com.example.camera.CameraActivity.class);
		    	intent.putExtra("currentPageNumber",parentActivityContext.currentPageNumber);
		    	intent.putExtra("currentSlipId",parentActivityContext.currentSlipId);
		        startActivityForResult(intent,parentActivityContext.BATCH_SCAN_REQUEST_CODE);
		    }
		});
		handler.postDelayed(update_details,100);
		return rootView;
	}
	
	public Runnable update_details=new Runnable() {
    	
    	
    	@SuppressWarnings("deprecation")
		public void run() {
    		String ungrouped_directory=MainActivity.default_folder_location+"ungrouped";
    		File file=new File(ungrouped_directory);
    		if(parentActivityContext.currentSlipId.equals("ungrouped")) {
	    		if(!file.isDirectory()) {
	    			new File(ungrouped_directory+"/menu/").mkdirs();
	    			new File(ungrouped_directory+"/pic/").mkdirs();
	    			parentActivityContext.currentPageNumber=1;
	    		}	
	    		else {
	    			parentActivityContext.currentPageNumber=new File(ungrouped_directory+"/menu/").list().length+1;
	    			parentActivityContext.currentPicNumber=new File(ungrouped_directory+"/pic/").list().length+1;
	    		}
	  
    		}	
	    /*	if(!parentActivityContext.menuName.equals("ungrouped")) {
	    		page.setEnabled(true);
	    	}
	    	else {
	    		page.setEnabled(false);
	    	}*/
    		TextView slipId=(TextView) myView.findViewById(R.id.currentSlipId);
	    	slipId.setText(parentActivityContext.currentSlipId);
	    	TextView savingLocation=(TextView) myView.findViewById(R.id.defaultSavingLocation);
	    	savingLocation.setText(MainActivity.default_folder_location+parentActivityContext.currentSlipId+"/");
	    	TextView slipScannedNumber=(TextView) myView.findViewById(R.id.slipScannedNumber);
	    	slipScannedNumber.setText(parentActivityContext.sessionMenuScanned.size()+"");
	    	int totalScannedPages = 0;
	    	Iterator<Map.Entry<String, Integer>> sessionMenuScanned = parentActivityContext.sessionMenuScanned.entrySet().iterator();
		    while (sessionMenuScanned.hasNext()) {
		        Map.Entry<String,Integer> pairs = (Map.Entry<String,Integer>)sessionMenuScanned.next();
		        totalScannedPages+=Integer.valueOf(pairs.getValue().toString());
		    }
		    
		    Iterator<Map.Entry<String, Integer>> it = parentActivityContext.sessionPicCaptured.entrySet().iterator();
		    int totalCapturedPics=0;
		    while (it.hasNext()) {
		        Map.Entry<String,Integer> pairs = (Map.Entry<String,Integer>)it.next();
		        totalCapturedPics+=Integer.valueOf(pairs.getValue().toString());
		    }
	    	TextView scannedMenuPageNumber=(TextView) myView.findViewById(R.id.scannedMenuPageNumber);
	    	scannedMenuPageNumber.setText(totalScannedPages+"");
	    	TextView capturedPicNumber=(TextView) myView.findViewById(R.id.capturedPicNumber);
	    	capturedPicNumber.setText(totalCapturedPics+"");
	    	TextView ip=(TextView) myView.findViewById(R.id.ip);
	    	String IP;
	    /*	WifiManager wim= (WifiManager) parentActivityContext.getSystemService(Context.WIFI_SERVICE);
	    	List<WifiConfiguration> l =  wim.getConfiguredNetworks(); 
	        WifiConfiguration wc = l.get(0);
	    	IP=Formatter.formatIpAddress(wim.getConnectionInfo().getIpAddress());
	    	ip.setText(IP);*/
    	}
	};	
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	

    	if(requestCode==parentActivityContext.BATCH_SCAN_REQUEST_CODE) {
    		if (resultCode == MainActivity.RESULT_OK) {
	            // Image captured and saved to fileUri specified in the Intent
        		int temp=intent.getExtras().getInt("numPageScanned");
        		parentActivityContext.ct.showToast(parentActivityContext,"total "+temp+" pages were scanned");
        		parentActivityContext.currentPageNumber=parentActivityContext.currentPageNumber+temp;
        		Integer temp_page=(Integer) parentActivityContext.sessionMenuScanned.get(parentActivityContext.currentSlipId);
        		if(temp_page!=null) {
        			int valueHolder=Integer.valueOf(temp_page.toString())+temp;
        			//parentActivityContext.ct.showToast(parentActivityContext,""+valueHolder);
        			parentActivityContext.sessionMenuScanned.put(parentActivityContext.currentSlipId,Integer.valueOf(valueHolder));
        			
        		}	
        		else {
        			parentActivityContext.sessionMenuScanned.put(parentActivityContext.currentSlipId,temp);
        		}	
        			handler.postDelayed(update_details,100);
        			
        			
	        } else if (resultCode ==MainActivity.RESULT_CANCELED) {
	        	parentActivityContext.ct.showToast(parentActivityContext,"something uncatched occoured");
	        } else {
	            // Image capture failed, advise user
	        }
    	
    	}
        if(requestCode ==parentActivityContext.DETECT_BAR_CODE_REQUEST_CODE) {
            if (resultCode == MainActivity.RESULT_OK) {
            	String result = intent.getStringExtra("SCAN_RESULT");
                File f = new File(MainActivity.default_folder_location+result);
                if(f.isDirectory()) {
                	parentActivityContext.currentSlipId=result;
	              //  page.setEnabled(true);
	                parentActivityContext.startingPageNumber=new File(MainActivity.default_folder_location+result+"/menu/").list().length+1;
	                parentActivityContext.startingPicNumber=new File(MainActivity.default_folder_location+result+"/pic/").list().length+1;
                }
                else {
   	                new File(MainActivity.default_folder_location+result+"/menu/").mkdirs(); 
   	                new File(MainActivity.default_folder_location+result+"/pic/").mkdirs();
   	               // ct.showToast(this,""+directory.getAbsolutePath());
   	                parentActivityContext.currentSlipId=result;
   	               // page.setEnabled(true);
	                parentActivityContext.startingPageNumber=1;
	                parentActivityContext.startingPicNumber=1;
	                parentActivityContext.sessionMenuScanned.put(parentActivityContext.currentSlipId, 0);
                }    
                parentActivityContext.currentPageNumber=parentActivityContext.startingPageNumber;
                parentActivityContext.currentPicNumber=parentActivityContext.startingPicNumber;
                handler.postDelayed(update_details,100);
            } 
            else if (resultCode == MainActivity.RESULT_CANCELED) {
            	parentActivityContext.ct.showToast(parentActivityContext,"Scan Menu id first or enter");
            }
        }
        if(requestCode ==parentActivityContext.NORMAL_IMAGE_CAPTURE) {
            if (resultCode == MainActivity.RESULT_OK) {
           
            	Integer num= parentActivityContext.sessionPicCaptured.get(parentActivityContext.currentSlipId);
        		if(num!=null) {
        			parentActivityContext.sessionPicCaptured.put(parentActivityContext.currentSlipId,num+1);
        			//parentActivityContext.ct.showToast(parentActivityContext,""+valueHolder);
        		}
        		else {
        			parentActivityContext.sessionPicCaptured.put(parentActivityContext.currentSlipId,1);
        		}
        		
                handler.postDelayed(update_details,100);
            } 
            else if (resultCode == MainActivity.RESULT_CANCELED) {
            	parentActivityContext.ct.showToast(parentActivityContext,"No picture was captured");
            }
        }
    }

	
	 @Override
	 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	        // TODO Auto-generated method stub
	        super.onCreateOptionsMenu(menu, inflater);
	        inflater.inflate(R.menu.main, menu);
	    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {

        case R.id.action_settings:
        	Intent i = new Intent(parentActivityContext, com.example.setting.SettingsActivity.class);
            startActivityForResult(i,parentActivityContext.SETTING_SET_CODE);
            return true;
       
        }
        return true;
    }
	
	 @Override
	    public void setUserVisibleHint(boolean isVisibleToUser) {
	        super.setUserVisibleHint(isVisibleToUser);
	        if(isVisibleToUser) {
	        	//parentActivityContext.ct.showToast(parentActivityContext,"jhdj jhnd ");
	        	handler=new Handler();
	        	handler.postDelayed(update_details,100);
	        	//parentActivityContext.viewPager.setCurrentItem(parentActivityContext.getActionBar().getSelectedNavigationIndex());
	        }
	    }

}
