package com.example.main;

import com.example.nevigation.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class CornerCircle extends View {

	private float x,y,radius;
	private Paint paintDrawing;
	private Paint paintWriting;
	private int num;
	public CornerCircle(Context context,float x ,float y,float radius,int num) {
		super(context);
		this.x=x;
		this.y=y;
		this.radius=radius;
		this.num=num;
		paintDrawing=new Paint();
	    paintWriting=new Paint();
	    paintWriting.setColor(Color.WHITE);
	    paintWriting.setTextSize(20);
	    paintWriting.setAntiAlias(true);
		 
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onDraw(Canvas canvas) {

		canvas.drawCircle(x,y,radius,paintDrawing);
		canvas.drawText(num+"",x,y,paintWriting);
	}
	
	@Override 
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){

	   super.onMeasure(widthMeasureSpec,heightMeasureSpec);
	}
	
	
	
	
	
}
