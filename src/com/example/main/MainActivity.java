package com.example.main;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.opencv.android.OpenCVLoader;

import com.example.adapters.TabsPagerAdapter;
import com.example.camera.metaData;
import com.example.nevigation.R;




import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;


public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {
	
	
	//static initialization of openCV
    static {
        if(!OpenCVLoader.initDebug()) {
            Log.d("ERROR", "Unable to load OpenCV");
        } else {
            Log.d("SUCCESS", "OpenCV loaded");
        }
    } 
    
	public int BATCH_SCAN_REQUEST_CODE=100;
	public int CAPTURE_IMAGE_REPLACE_CODE=500;
	public int DETECT_BAR_CODE_REQUEST_CODE=200; 
	public int NORMAL_IMAGE_CAPTURE=400;
	public int SETTING_SET_CODE=300;
	public ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	public static String default_folder_location;
	public static MainActivity myObject;
	public CustomToast ct;
	public int startingPageNumber=1;
	public int currentPageNumber=1;
	public int startingPicNumber=1;
	public int currentPicNumber=1;
	public String currentSlipId="ungrouped";
	private String ungrouped_directory;
	public Map<String,Integer> sessionMenuScanned=new HashMap<String,Integer>();
	public Map<String,Integer> sessionPicCaptured=new HashMap<String,Integer>();
	private String[] tabs = { "Scan", "Session", "Browse" };
    public static metaData imageData;

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		imageData=new metaData(MainActivity.this);
		myObject=this;
		ct=new CustomToast();
		default_folder_location=Environment.getExternalStorageDirectory().
	        	 getAbsolutePath() +"/scan/";
		
		ungrouped_directory=default_folder_location+currentSlipId;
		File file=new File(ungrouped_directory);
		if(!file.isDirectory()) {
			new File(ungrouped_directory+"/menu/").mkdirs();
			new File(ungrouped_directory+"/pic/").mkdirs();
			currentPageNumber=1;
		}	
		else {
			currentPageNumber=file.list().length+1;
		}
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);		

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
	
	
	
	
	
	


}
