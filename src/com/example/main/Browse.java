package com.example.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import com.example.adapters.SlipAdapter;
import com.example.item.SlipItem;
import com.example.nevigation.R;
import com.example.nevigation.R.color;



import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.GridView;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Browse extends Fragment {
	private File file;
	private AlertDialog adOption;
	private String currentSlipId;
	private MainActivity parentActivityContext;
	private View myView;
	private Handler handler;
	private int LIST_PAGE_CODE=101;
	private ImageView discardImage;
	private ImageView addImage;
	private ImageView editImage;
	private GridView gridSlip;
    
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		 parentActivityContext=MainActivity.myObject;
		 handler=new Handler();
		 View rootView = inflater.inflate(R.layout.browse_frag, container, false);
		 myView=rootView;
		 discardImage=(ImageView) myView.findViewById(R.id.discardIcon);
		 addImage=(ImageView) myView.findViewById(R.id.addIcon);
		 editImage=(ImageView) myView.findViewById(R.id.editIcon);
		 discardImage.setOnDragListener(new slipIdDragListner()) ;
		 addImage.setOnDragListener(new slipIdDragListner()) ;
		 editImage.setOnDragListener(new slipIdDragListner()) ;
		 handler.postDelayed(list_menu,200);
		 // parentActivityContext.ct.showToast(parentActivityContext,"lll");
    	 return rootView;
    }   
	
	 
	public Runnable list_menu=new Runnable() {
    	
    	
    	public void run() {
    	
    		 gridSlip = (GridView) myView.findViewById(R.id.gridMenu);
	       	 file = new File(MainActivity.default_folder_location);
	       	 String[] names =file.list();
	       	 File menuFolder;
	       	 File picFolder;
	       	 final ArrayList<SlipItem> list = new ArrayList<SlipItem>();
	       	 if(names!=null) {
	   		     for(String name:names)
	   		     {
	   		    	menuFolder=new File(MainActivity.default_folder_location+name+"/menu/");
	   		    	picFolder=new File(MainActivity.default_folder_location+name+"/pic/");
	   		    	if(menuFolder.isDirectory() && picFolder.isDirectory()) {
	   		    		list.add(new SlipItem(menuFolder.list().length,picFolder.list().length,name));
	   		    	}	
	   	        
	   		     }  
	       	 }    
	       	 TextView numMenu=(TextView) myView.findViewById(R.id.num_menu);
	         numMenu.setText(list.size()+" slip found");
	         
	       	 SlipAdapter adapter = new SlipAdapter(parentActivityContext,
	                    R.layout.menu_list_item,list);
	       	 
	       	gridSlip.setAdapter(adapter); 
	        Animation anim = AnimationUtils.loadAnimation(parentActivityContext, R.anim.fly_in_from_center);
            gridSlip.setAnimation(anim);
            anim.start();
	       	/*   	 gridView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View view, MotionEvent motionEvent) {
					
					if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					     discardImage.setVisibility(View.VISIBLE);
					     ClipData data = ClipData.newPlainText("value", "xyz");
					     DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					     view.startDrag(data, shadowBuilder,null, 0);
					     return true;
					}
					return false;
				}
	       		 
	       	 });*/
	       	gridSlip.setOnItemClickListener(new OnItemClickListener() {
	   

					@Override
					public void onItemClick(AdapterView<?> arg0, final View view,
							final int position, long arg3) {
						final CharSequence[] items={"menu","pic"};
				        AlertDialog.Builder builderOption=new AlertDialog.Builder(parentActivityContext);
				    	builderOption.setTitle(currentSlipId);
				    	/*builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {}
						});*/
				    	
				        builderOption.setNegativeButton("Cancel",
			                    new DialogInterface.OnClickListener() {
	                     public void onClick(DialogInterface dialog,int which) {
	                     	adOption.cancel();
	                     }
				        });
				     	builderOption.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
							
			       				Intent intent=new Intent(parentActivityContext,ListPageActivity.class);
			       				intent.putExtra("currentSlipId",((SlipItem) gridSlip.getItemAtPosition(position)).getMenuName());
			       				intent.putExtra("limitedResult",false);
			       				intent.putExtra("folderType",items[which]);
			       				startActivityForResult(intent,LIST_PAGE_CODE);
			       				//overridePendingTransition(R.anim.activity_in,R.anim.activity_out);
							}
					   });
				     	
				     	adOption=builderOption.create();
				     	adOption.show();
					}  	
	       	 });	
	       	 
	       	gridSlip.setLongClickable(true);
	       	gridSlip.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
	
	
				@Override
				public boolean onItemLongClick(AdapterView<?> arg0,
						View view, int arg2, long arg3) {
					
					 ClipData data = ClipData.newPlainText("slipId",((SlipItem)gridSlip.getItemAtPosition(arg2)).getMenuName());
					 DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
				     view.startDrag(data, shadowBuilder,null, 0);
				     discardImage.setVisibility(View.VISIBLE);
					 addImage.setVisibility(View.VISIBLE);
					 editImage.setVisibility(View.VISIBLE);

					return false;	
	       	 
				}

	       	});	 
    	
	}
	
	};
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	
		if(requestCode==LIST_PAGE_CODE) {
				adOption.dismiss();
    			handler.postDelayed(list_menu,100);
    		
		}	
	}
	
	public class slipIdDragListner implements OnDragListener {

		@Override
		public boolean onDrag(View droppedOn, DragEvent dragEvent) {
			// TODO Auto-generated method stub
				ImageView tempImageView=(ImageView) myView.findViewById(droppedOn.getId());
				switch (dragEvent.getAction()) {
				
		            case DragEvent.ACTION_DRAG_ENTERED:
		            	android.view.ViewGroup.LayoutParams layoutParams =tempImageView.getLayoutParams();
		            	layoutParams.width = 200;
		            	layoutParams.height = 200;
		            	tempImageView.setLayoutParams(layoutParams);
		                //v.setBackgroundColor(Color.GRAY);
		                return false;
		
		            case DragEvent.ACTION_DRAG_ENDED:
		            	 gridSlip.setBackgroundColor(getResources().getColor(R.color.background));
		            	 discardImage.setVisibility(View.INVISIBLE);
		            	 addImage.setVisibility(View.INVISIBLE);
		            	 editImage.setVisibility(View.INVISIBLE);
		            	 gridSlip.setAlpha((float) 1);
		                //v.setBackgroundColor(Color.TRANSPARENT);
		                return true;
		
		            case DragEvent.ACTION_DRAG_STARTED:
		            	 //GridView.LayoutParams params=(LayoutParams) gridSlip.getLayoutParams();
		            	// params.
		            
		            	gridSlip.setBackgroundColor(getResources().getColor(R.color.trans));
		            	gridSlip.setAlpha((float) .5);
		            	/*  Animation anim = AnimationUtils.loadAnimation(parentActivityContext, R.anim.fly_in_from_center);
		            	  
		            	  gridSlip.setAnimation(anim);
	                      anim.start();
	                      parentActivityContext.ct.showToast(parentActivityContext,"yes");*/
		                 return true;
		
		            case DragEvent.ACTION_DRAG_LOCATION:
		                //v.setVisibility(View.VISIBLE);
		                return false;
		                
		            case DragEvent.ACTION_DRAG_EXITED:
		            	layoutParams =tempImageView.getLayoutParams();
		            	layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
		            	layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
		            	tempImageView.setLayoutParams(layoutParams);
		                return false;
		                // return processDragStarted(event);
		            case DragEvent.ACTION_DROP:
		            	currentSlipId=dragEvent.getClipData().getItemAt(0).getText().toString();
		            	if(droppedOn.getId()==R.id.discardIcon) {
			            	parentActivityContext.ct.showToast(parentActivityContext,currentSlipId);
			            	File file=new File(MainActivity.default_folder_location+currentSlipId+"/");
				            try {
				            	if(currentSlipId.equals(parentActivityContext.currentSlipId)) {
				            		parentActivityContext.currentSlipId="ungrouped";
				            	}
				            	FileUtils.deleteDirectory(file);
				            	//parentActivityContext.ct.showToast(parentActivityContext,currentSlipId+" deleted");
				            }
				            catch(IOException ex)  {
				            	
				            	parentActivityContext.ct.showToast(parentActivityContext,"could not deleted"+ex.getMessage());
				            }
				            discardImage.setVisibility(View.INVISIBLE);
				            handler.postDelayed(list_menu,100);
		            	}
		            	else if(droppedOn.getId()==R.id.addIcon) {
		            		
		            		parentActivityContext.currentSlipId=currentSlipId;
							parentActivityContext.currentPageNumber=new File(MainActivity.default_folder_location+currentSlipId).list().length+1;
							parentActivityContext.startingPageNumber=parentActivityContext.currentPageNumber;
							parentActivityContext.viewPager.setCurrentItem(0,true);
		            		
		            	}
		            	else if(droppedOn.getId()==R.id.editIcon) {
		            		
		            		 final EditText input = new EditText(parentActivityContext); 
							 AlertDialog.Builder rename_alert=new AlertDialog.Builder(parentActivityContext);
						     rename_alert.setTitle("new name");
						     rename_alert.setView(input);
						     rename_alert.setPositiveButton("OK",
					                    new DialogInterface.OnClickListener() {
			                        public void onClick(DialogInterface dialog,int which) {
			                        	
			                            File old_image_file = new File(MainActivity.default_folder_location+currentSlipId+"/");
			                            String new_slip_id=input.getText().toString();
			                            // File (or directory) with new name
			                            File new_image_file = new File(MainActivity.default_folder_location+new_slip_id+"/");
			                            if(new_image_file.exists()) {
			                            	
			                            	parentActivityContext.ct.showToast(parentActivityContext,"Menu exists already");
			                            }
			                            else {
			                            // Rename file (or directory)
				                            boolean success = old_image_file.renameTo(new_image_file);
				                            if (!success) {
				                            	parentActivityContext.ct.showToast(parentActivityContext,"not renamed");
				                            }
				                            else {
				                            	
				                            	parentActivityContext.ct.showToast(parentActivityContext,"renamed");
				                            	handler.postDelayed(list_menu,100);
				                            }
			                            }  
			                        }
						     });
						     
						     rename_alert.setNegativeButton("CANCEL",
					                    new DialogInterface.OnClickListener() {
			                        public void onClick(DialogInterface dialog,int which) {
			                        	
			                        }
						     });
							rename_alert.show();
		            	}
		            	layoutParams =tempImageView.getLayoutParams();
		            	layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
		            	layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
		            	tempImageView.setLayoutParams(layoutParams);

	
	           }
				return true;
		
		}
		 
		
	}

} 