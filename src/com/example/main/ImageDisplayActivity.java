package com.example.main;

import java.util.ArrayList;
import java.util.List;


import com.example.nevigation.R;
import com.example.processing.ImageProcessing;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ImageDisplayActivity extends Activity implements OnClickListener{

	private String currentSlipId;
	private String default_folder_location;
	private String pageName;
	private int pageNumber;
	private ImageView cornerImage1;
	private ImageView cornerImage2;
	private ImageView cornerImage3;
	private ImageView cornerImage4;
	private ImageProcessing imageProcessing;
	private RelativeLayout.LayoutParams lp;
	private ImageView mainImage;
	private List<Point> initialCornerPoints=new ArrayList<Point>();
	private ProgressDialog pDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_display);
		currentSlipId=getIntent().getStringExtra("currentSlipId");
		default_folder_location=getIntent().getStringExtra("default_folder_location");
		pageName=getIntent().getStringExtra("pageNum");
		pageNumber=extractPageNum(pageName);
		cornerImage1=(ImageView) findViewById(R.id.cornerImage1);
		cornerImage2=(ImageView) findViewById(R.id.cornerImage2);
		cornerImage3=(ImageView) findViewById(R.id.cornerImage3);
		cornerImage4=(ImageView) findViewById(R.id.cornerImage4);
		cornerImage1.setOnTouchListener(new circleTouchListener());
		cornerImage2.setOnTouchListener(new circleTouchListener());
		cornerImage3.setOnTouchListener(new circleTouchListener());
		cornerImage4.setOnTouchListener(new circleTouchListener());
		displayImage();
		/*Button nextButton=(Button)findViewById(R.id.nextButton);
		Button prevButton=(Button)findViewById(R.id.prevButton);
		nextButton.setOnClickListener(this);
		prevButton.setOnClickListener(this);*/
		Button transform=(Button)findViewById(R.id.transform);
		transform.setOnClickListener(this);
		mainImage=(ImageView) findViewById(R.id.mainImageViewId);
		mainImage.setOnDragListener(new mainImageDragListner());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_display, menu);
		return true;
	}

	
	/*
	@Override
	public void onClick(View view) {
		
		if(view.getId()==R.id.nextButton) {
			
			int fileCount=new File(default_folder_location+currentSlipId+"/menu/").list().length;
			if(pageNumber<fileCount) {
				pageNumber++;
				handler.postDelayed(display_image,100);
				
			}
		}
		else if(view.getId()==R.id.prevButton) {
			
			if(pageNumber>1) {
				pageNumber--;
				handler.postDelayed(display_image,100);
			}	
			
		}
	}
	*/
	public int extractPageNum(String string) {
		
			return Integer.parseInt(string.substring(0,string.indexOf(".jpg")));
		
	}
	
	    	
	    	public void displayImage() {
	    		
	    		ImageView imgView;
	    		imgView = (ImageView) findViewById(R.id.mainImageViewId);
	    		imgView.setImageBitmap(BitmapFactory.decodeFile(default_folder_location+currentSlipId+"/menu/"+pageNumber+".jpg"));
	    		pDialog=ProgressDialog.show(ImageDisplayActivity.this,"","loading pages",true);
	    		Log.d("msg","call here");
	    		getActionBar().setTitle((currentSlipId+"/menu/"+pageNumber));
	    		imageProcessing=new ImageProcessing(ImageDisplayActivity.this,default_folder_location+currentSlipId+"/menu/"+pageNumber+".jpg");
	    		initialCornerPoints=imageProcessing.getAndroidPoints(imageProcessing.getMenuVertices(imageProcessing.imageMat));
	    		Drawable drawable = imgView.getDrawable();
	    		Rect imageBounds = drawable.getBounds();

	    		//original height and width of the bitmap
	    		int intrinsicHeight = drawable.getIntrinsicHeight();
	    		int intrinsicWidth = drawable.getIntrinsicWidth();

	    		//height and width of the visible (scaled) image
	    		int scaledHeight = imageBounds.height();
	    		int scaledWidth = imageBounds.width();

	    		//Find the ratio of the original image to the scaled image
	    		//Should normally be equal unless a disproportionate scaling
	    		//(e.g. fitXY) is used.
	    		if(scaledHeight!=0) {
	    			float heightRatio = intrinsicHeight / scaledHeight;
	    		}	
	    		if(scaledWidth!=0) {
	    			float widthRatio = intrinsicWidth / scaledWidth;
	    		}
	    		
	    		if(initialCornerPoints.size()==0) {
	    			initialCornerPoints.add(new Point(0,0));
	    			initialCornerPoints.add(new Point(100,800));
	    			initialCornerPoints.add(new Point(200,400));
	    			initialCornerPoints.add(new Point(600,700));
	    		}
	    		Log.d("bounds",""+scaledHeight+"->"+scaledWidth+" "+imageBounds.left+" "+imageBounds.right+" "+imageBounds.top+" "+imageBounds.bottom+" ");
	    		initialCornerPoints.set(0,new Point(initialCornerPoints.get(0).x+imageBounds.left,initialCornerPoints.get(0).y+imageBounds.top));
	    		initialCornerPoints.set(1,new Point(initialCornerPoints.get(1).x+imageBounds.left,initialCornerPoints.get(1).y+imageBounds.top));
	    		initialCornerPoints.set(2,new Point(initialCornerPoints.get(2).x+imageBounds.left,initialCornerPoints.get(2).y+imageBounds.top));
	    		initialCornerPoints.set(3,new Point(initialCornerPoints.get(3).x+imageBounds.left,initialCornerPoints.get(3).y+imageBounds.top));
	    		Log.d("result",dipToPixels(ImageDisplayActivity.this,100)+"");
	    	    Log.d("coordinate1",(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(0).x)+" "+(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(0).y));
	    	    Log.d("coordinate2",(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(1).x)+" "+(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(1).y));
	    	    Log.d("coordinate3",(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(2).x)+" "+(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(2).y));
	    	    Log.d("coordinate4",(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(3).x)+" "+(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(3).y));
	    	    
	    	    lp = (RelativeLayout.LayoutParams)cornerImage1.getLayoutParams();
	    		lp.setMargins((int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(0).x),(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(0).y),0,0);
	    		cornerImage1.setLayoutParams(lp);
	    		lp = (RelativeLayout.LayoutParams)cornerImage2.getLayoutParams();
	    		lp.setMargins((int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(1).x),(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(1).y),0,0);
	    		cornerImage2.setLayoutParams(lp);
	    		lp = (RelativeLayout.LayoutParams)cornerImage3.getLayoutParams();
	    		lp.setMargins((int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(2).x),(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(2).y),0,0);
	    		cornerImage3.setLayoutParams(lp);
	    		lp = (RelativeLayout.LayoutParams)cornerImage4.getLayoutParams();
	    		lp.setMargins((int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(3).x),(int)dipToPixels(ImageDisplayActivity.this,initialCornerPoints.get(3).y)-25,0,0);
	    		cornerImage4.setLayoutParams(lp);
	    		pDialog.dismiss();
	    	}
    
    
    public class circleTouchListener implements OnTouchListener {
    	
    	@Override
    	public boolean onTouch(View view, MotionEvent event) {
    		

			 Log.d("ImageDisplay",view.getId()+"");
			 ClipData data = ClipData.newPlainText("id",view.getId()+"");
			 DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
		     view.startDrag(data, shadowBuilder,null, 0);
			 return false;

    	}
    };
    public class mainImageDragListner implements OnDragListener {
		public boolean onDrag(View droppedOn, DragEvent dragEvent) {
			// TODO Auto-generated method stub
	
				switch (dragEvent.getAction()) {
				
		            case DragEvent.ACTION_DRAG_ENTERED:
	
		                return false;
		
		            case DragEvent.ACTION_DRAG_ENDED:
		            //	Log.d("ended drag",dragEvent.getClipData().toString());
		                return true;
		
		            case DragEvent.ACTION_DRAG_STARTED:
		            	return true;
		            
		            case DragEvent.ACTION_DROP:
		            	Log.d("ended drag",dragEvent.getClipData().toString());
		            	Log.d("coorinates",dragEvent.getX()+"->"+dragEvent.getY());
		            	ImageView tempImage=(ImageView) findViewById(Integer.valueOf(dragEvent.getClipData().getItemAt(0).getText().toString()));
		            	lp=(android.widget.RelativeLayout.LayoutParams) tempImage.getLayoutParams();
		            	lp.setMargins((int)dragEvent.getX()-25,(int)dragEvent.getY()-25,0,0);
		            	tempImage.setLayoutParams(lp);
		            	return true;
		            	
				}
				return true;
	    }			

    }
    
    public static int dipToPixels(Context context, float dipValue) {
    	  // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dipValue / scale + 0.5f);
    }

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view.getId()==R.id.transform) {
			
			
		}
	}

}
