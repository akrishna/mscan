package com.example.main;

import org.opencv.android.OpenCVLoader;

import com.example.nevigation.R;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class SplashActivity extends Activity {


    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
         WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 setContentView(R.layout.activity_splash);
	    new Handler().postDelayed(new Runnable() {
            public void run() {

                    /* Create an intent that will start the main activity. */
                    Intent mainIntent = new Intent(SplashActivity.this,
                            MainActivity.class);
                    //SplashScreen.this.startActivity(mainIntent);
                    startActivity(mainIntent);
                    /* Finish splash activity so user cant go back to it. */
                    SplashActivity.this.finish();

                    /* Apply our splash exit (fade out) and main
                       entry (fade in) animation transitions. */
                    overridePendingTransition(R.anim.activity_in,R.anim.activity_out);
            }
    }, 1000); 
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

}
