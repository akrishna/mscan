package com.example.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import com.example.adapters.PageAdapter;
import com.example.item.PageItem;
import com.example.nevigation.R;
import com.example.processing.ImageProcessing;

import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ListPageActivity extends Activity {

	private Handler handler;
	private String currentSlipId;
	private int CAPTURE_IMAGE_REPLACE_CODE=500;
	private String currentFileName;
	private String default_folder_location;
	private AlertDialog adOption;
	private CustomToast ct;
	private int count=0;
	private String folderType;
	private boolean limited=false;
	private int THUMBSIZE=64;
	private ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_page);
		handler=new Handler();
		currentSlipId=getIntent().getStringExtra("currentSlipId");
		folderType=getIntent().getStringExtra("folderType");
		ct=new CustomToast();
		default_folder_location=MainActivity.default_folder_location;
		if(getIntent().getExtras().getBoolean("limitedResult")) {
			limited=true;
			count=getIntent().getExtras().getInt("pageCount");
		}
		pDialog=ProgressDialog.show(ListPageActivity.this,"","loading pages",true);
		pDialog.setIndeterminate(false);
		handler.postDelayed(list_pages,200);	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_page, menu);
		return true;
	}
	
	public void viewImage(String pageNum) {
		
		
		Intent intent = new Intent(ListPageActivity.this,ImageDisplayActivity.class);
		//intent.setDataAndType(Uri.parse("file://"+default_folder_location+image_path), "image/*");
		intent.putExtra("currentSlipId",currentSlipId);
		intent.putExtra("pageNum",pageNum);
		intent.putExtra("default_folder_location",default_folder_location);
		startActivity(intent);
	}
	
	public void viewInGallery(String fileAdd) {
		
		
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://"+fileAdd), "image/*");
		startActivity(intent);
	}
	
	public void rotate(String slipId,String page,int rotationAngle) {
		
		ImageProcessing.rotateImage(default_folder_location+slipId+"/"+folderType+"/"+page, rotationAngle);
	}
	
	public Runnable list_pages=new Runnable() {
    	
    	
    	public void run() {
    		 final GridView pageView = (GridView) findViewById(R.id.gridPage);
        	 File file = new File(default_folder_location+currentSlipId+"/"+folderType+"/");
        	 getActionBar().setTitle(currentSlipId+"/"+folderType+"("+file.list().length+")");
        	 File[] files =file.listFiles();
        	 final ArrayList<PageItem> list = new ArrayList<PageItem>();
        	 if(files!=null) {
        		 if(limited!=true) {
		 		     for(File tempFile : files)
				     {		
		 		    	 	if(folderType.equals("menu")) {
		 		    	 		list.add(new PageItem(tempFile.getName(),ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(tempFile.getAbsolutePath()), THUMBSIZE, THUMBSIZE)));
		 		    	 	}
		 		    	 	else {
		 		    	 		list.add(new PageItem(tempFile.getName()));
		 		    	 	}
				        
				     }  
        		 }
        		 else {
        			 Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
        			 for(int i=0;i<count && i< files.length;i++) {
        				 list.add(new PageItem(files[i].getName()));
        			 }
        		 }
        	 }
        	 
        	 TextView tv=(TextView) findViewById(R.id.num_page);
	             tv.setText(list.size()+" pages");
        	PageAdapter adapter = new PageAdapter(ListPageActivity.this,
                    R.layout.page_list_item,list);
        	 
        	 pageView.setAdapter(adapter); 
        	 
        	 pDialog.dismiss();
        	/* Toast.makeText(MainActivity.this, ""+list2.size()
	                     , Toast.LENGTH_LONG).show(); */
        	 pageView.setOnItemClickListener(new OnItemClickListener() {
        			public void onItemClick(AdapterView<?> parent_page, View view,
        					int position, long id) {
        			    // When clicked, show a toast with the TextView text
        				viewInGallery(default_folder_location+currentSlipId+"/"+folderType+"/"+((PageItem) pageView.getItemAtPosition(position)).getPageName());
        			  
        			}

	
        		});
        	 pageView.setLongClickable(true);
        	 pageView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {


				@Override
				public boolean onItemLongClick(final AdapterView<?> arg0,
						View arg1,final int position, long arg3) {
					final String imageName=((PageItem) pageView.getItemAtPosition(position)).getPageName();
					currentFileName=default_folder_location+currentSlipId+"/"+folderType+"/"+imageName;
					final CharSequence[] items={"Replace","Process","Rotate","Rename","View","Delete","All Delete"};
			        AlertDialog.Builder builderOption=new AlertDialog.Builder(ListPageActivity.this);
			    	builderOption.setTitle("Edit");
			    	/*builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {}
					});*/
			    builderOption.setNegativeButton("Cancel",
		                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                        	adOption.cancel();
                        }
			     });
			     	builderOption.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							if("Replace".equals(items[which]))
							{
								 Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					             File imageFile=new File(currentFileName);
					        	 Uri uriSavedImage = Uri.fromFile(imageFile);
					        	 imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
					        	 imageIntent.putExtra("IMAGE_NAME", imageName);
					        	 startActivityForResult(imageIntent,CAPTURE_IMAGE_REPLACE_CODE);
								 //ct.showToast(MainActivity.this,"image replaced"+image_path);
								 adOption.cancel();
							}
							else if("Process".equals(items[which]))
							{
								 ImageProcessing process=new ImageProcessing(ListPageActivity.this,currentFileName);
						    	 process.execute();
								 adOption.cancel();
							}
							else if("Rename".equals(items[which]))
							{	
								 final EditText input = new EditText(ListPageActivity.this); 
								 AlertDialog.Builder rename_alert=new AlertDialog.Builder(ListPageActivity.this);
							     rename_alert.setTitle("new name");
							     rename_alert.setView(input);
							     rename_alert.setPositiveButton("OK",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	
				                            File old_image_file = new File(currentFileName);
				                            String new_image_name=input.getText().toString();
				                            // File (or directory) with new name
				                            if(!new_image_name.equals("")) {
					                            File new_image_file = new File(default_folder_location+currentSlipId+"/"+folderType+"/"+new_image_name+".jpg");
					                            if(new_image_file.exists()) {
					                            	
					                            	ct.showToast(ListPageActivity.this,"image exists already");
					                            }
					                            else {
					                            // Rename file (or directory)
						                            boolean success = old_image_file.renameTo(new_image_file);
						                            if (!success) {
						                            	ct.showToast(ListPageActivity.this,"not renamed");
						                            }
						                            else {
						                            	
						                            	ct.showToast(ListPageActivity.this,"renamed");
						                            	pDialog=ProgressDialog.show(ListPageActivity.this,"","loading pages",true);
						                            	pDialog.setIndeterminate(false);
						                            	handler.postDelayed(list_pages,100);
						                            }
					                            } 
				                            }
				                            adOption.cancel();
				                        }
							     });
							     
							     rename_alert.setNegativeButton("CANCEL",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	adOption.cancel();
				                        }
							     });
								rename_alert.show();
							}
							else if("Delete".equals(items[which]))
							{

					            File imageFile=new File(currentFileName);
					            if(imageFile.delete()) {
					            	
					            	ct.showToast(ListPageActivity.this,imageName+" deleted");
					            }
					            else {
					            	
					            	ct.showToast(ListPageActivity.this,"could not deleted");
					            }
					            pDialog=ProgressDialog.show(ListPageActivity.this,"","loading pages",true);
					            pDialog.setIndeterminate(false);
					            handler.postDelayed(list_pages,100);
								adOption.cancel();
							}
							else if("All Delete".equals(items[which])) {
								
								
								 AlertDialog.Builder all_delete_alert=new AlertDialog.Builder(ListPageActivity.this);
							     all_delete_alert.setTitle("confirm all delete ?");
							     all_delete_alert.setPositiveButton("OK",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	
				                           File directory=new File(default_folder_location+currentSlipId+"/"+folderType);
				                           for(String file:directory.list()) {
				                        	   
				                        	   File temp_image=new File(default_folder_location+currentSlipId+"/"+folderType+"/"+file);
				                        	   temp_image.delete();
				                           }
				                           pDialog=ProgressDialog.show(ListPageActivity.this,"","loading pages",true);
				                           pDialog.setIndeterminate(false);
				                           handler.postDelayed(list_pages,100);
										   adOption.cancel();
										   ct.showToast(ListPageActivity.this,"all deleted");
				                        }
				                        
				                        
							     });
							     all_delete_alert.setNegativeButton("Cancel",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	adOption.cancel();
				                        }
							     });
							     
							     all_delete_alert.show();
							}
							else if("View".equals(items[which])) {
								
							    viewImage(imageName);
							}
							else if("Rotate".equals(items[which])) {
		
								 LayoutInflater factory = LayoutInflater.from(ListPageActivity.this);
								 final View view = factory.inflate(R.layout.angle_prompt, null);
								 final SeekBar seekBar =(SeekBar) view.findViewById(R.id.angle);
								 final TextView angleTv =(TextView) view.findViewById(R.id.angleValue);
							     seekBar.setMax(360);
							     seekBar.setVisibility(View.VISIBLE);
							     seekBar.setProgress(1);
								 AlertDialog.Builder angle_alert=new AlertDialog.Builder(ListPageActivity.this);
							     angle_alert.setTitle("angle");
							     angle_alert.setView(view);
							     seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {       

							    	    @Override       
							    	    public void onStopTrackingTouch(SeekBar seekBar) {      
							    	        // TODO Auto-generated method stub      
							    	    }       

							    	    @Override       
							    	    public void onStartTrackingTouch(SeekBar seekBar) {     
							    	        // TODO Auto-generated method stub      
							    	    }       

							    	    @Override       
							    	    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {     
							    	        // TODO Auto-generated method stub      

							    	        angleTv.setText(progress+" degree Anti clock wise");

							    	    }       
							    	}); 
							     angle_alert.setPositiveButton("OK",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	
				                        	int angle=seekBar.getProgress();;
				                            rotate(currentSlipId,imageName,angle);
				                            adOption.cancel();
				                        }
							     });
							     
							     angle_alert.setNegativeButton("CANCEL",
						                    new DialogInterface.OnClickListener() {
				                        public void onClick(DialogInterface dialog,int which) {
				                        	adOption.cancel();
				                        }
							     });
								angle_alert.show();
							}
							
						}
						
					});
			     	adOption=builderOption.create();
			     	adOption.show();
			     	return false;
			    
				}
      
        		 	
        	 });

    	}
    };
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();      
    }

}
