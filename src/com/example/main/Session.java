package com.example.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.example.adapters.SessionAdapter;
import com.example.item.SessionItem;
import com.example.nevigation.R;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Session extends Fragment {

	private Handler handler;
	private View myView;
	private MainActivity parentActivityContext;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		parentActivityContext=MainActivity.myObject;
	//	parentActivityContext.ct.showToast(parentActivityContext,parentActivityContext.sessionScan.size()+"session");
		View rootView = inflater.inflate(R.layout.session_frag, container, false);
		myView=rootView;
		handler=new Handler();
		handler.postDelayed(list_task,200);
		return rootView;
	}
	
	public Runnable list_task=new Runnable() {
	    	
	    	
	    	public void run() {
	    		final ArrayList<SessionItem> list = new ArrayList<SessionItem>();
	    		if(parentActivityContext.sessionMenuScanned.size()!=0) {
		    		Iterator<Map.Entry<String, Integer>> it = parentActivityContext.sessionMenuScanned.entrySet().iterator();
		    		    while (it.hasNext()) {
		    		        Map.Entry<String,Integer> pairs = (Map.Entry<String,Integer>)it.next();
		    		        list.add(new SessionItem(Integer.valueOf(pairs.getValue().toString()),pairs.getKey().toString(),"menu"));
		    		        if(parentActivityContext.sessionPicCaptured.get(pairs.getKey())!=null)
		    		        	list.add(new SessionItem(parentActivityContext.sessionPicCaptured.get(pairs.getKey()),pairs.getKey().toString(),"pic"));
		    		    }
	    		}
	    		else {
	    			Iterator<Map.Entry<String, Integer>> it = parentActivityContext.sessionPicCaptured.entrySet().iterator();
	    		    while (it.hasNext()) {
	    		        Map.Entry<String,Integer> pairs = (Map.Entry<String,Integer>)it.next();
	    		        list.add(new SessionItem(Integer.valueOf(pairs.getValue().toString()),pairs.getKey().toString(),"pic"));
	    		        if(parentActivityContext.sessionMenuScanned.get(pairs.getKey())!=null)
	    		        	list.add(new SessionItem(parentActivityContext.sessionMenuScanned.get(pairs.getKey()),pairs.getKey().toString(),"menu"));
	    		    }
	    			
	    		}
	    		
	    		//parentActivityContext.ct.showToast(parentActivityContext,parentActivityContext.sessionScan.size()+"");
	       	 	TextView numTask=(TextView) myView.findViewById(R.id.numTask);
	       	 	numTask.setText(parentActivityContext.sessionMenuScanned.size()+" record found");
	       	 	SessionAdapter adapter = new SessionAdapter(parentActivityContext,R.layout.session_list_item,list);
	       	 	final ListView listview = (ListView) myView.findViewById(R.id.listTask);
	       	 	listview.setAdapter(adapter); 
	       	 	listview.setOnItemClickListener(new OnItemClickListener() {
	       			public void onItemClick(final AdapterView<?> parent_page, View view,
	       					final int position, long id) {
	       				Intent intent=new Intent(parentActivityContext,ListPageActivity.class);
	       				intent.putExtra("currentSlipId",((SessionItem)listview.getItemAtPosition(position)).getSlipId());
	       				intent.putExtra("limitedResult",true);
	       				intent.putExtra("folderType",((SessionItem)listview.getItemAtPosition(position)).getFolderType());
	       				intent.putExtra("pageCount",((SessionItem)listview.getItemAtPosition(position)).getpageNum());
	       				startActivityForResult(intent,0);
	       			}
	       	 });	
	    	}
	    	
	};
	
	 @Override
	    public void setUserVisibleHint(boolean isVisibleToUser) {
	        super.setUserVisibleHint(isVisibleToUser);
	        if(isVisibleToUser) {
	        	//parentActivityContext.ct.showToast(parentActivityContext,"jhdj jhnd ");
	        	handler.postDelayed(list_task,100);
	        	//parentActivityContext.viewPager.setCurrentItem(parentActivityContext.getActionBar().getSelectedNavigationIndex());
	        }
	    }
	 
	
}	
	