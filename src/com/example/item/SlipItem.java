package com.example.item;

public class SlipItem {

	private int pageNum;
	private int picNum;
	private String slipId;
	
	public SlipItem(int pageNum,int picNum,String id) {
		
		this.pageNum=pageNum;
		this.picNum=picNum;
		slipId=id;
	}
	
	public int getPicNum() {
		
		return picNum;
	}
	
	public int getpageNum() {
		
		return pageNum;
	}
	public String getMenuName() {
		
		return slipId;
	}
	
	public void setPicNum(int num) {
		
		picNum=num;
	}
	public void setpageNum(int num) {
		
		pageNum=num;
	}
	public void setMenuName(String id) {
		
		slipId=id;
	}
}
