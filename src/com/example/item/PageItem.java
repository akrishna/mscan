package com.example.item;

import android.graphics.Bitmap;

public class PageItem {

	private String pageName;
	private Bitmap thumbnail;
	
	public PageItem(String name) {
		
		pageName=name;
	}
	public PageItem(String name,Bitmap thumb) {
		
		pageName=name;
		thumbnail=thumb;
	}
	
	public Bitmap getThumbnail() {
		
		return thumbnail;
	}
	public String getPageName() {
		
		return pageName;
	}
	
	public void setThumbnail(Bitmap map) {
		
		thumbnail=map;
	}
	public void setPageName(String name) {
		
		pageName=name;
	}
}
