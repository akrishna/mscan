package com.example.item;

public class SessionItem {

	private int pageNum;
	private String slipId;
	private String folderType;
	
	public SessionItem(int num,String id,String type) {
		
		pageNum=num;
		slipId=id;
		folderType=type;
	}
	
	public int getpageNum() {
		
		return pageNum;
	}
	public String getSlipId() {
		
		return slipId;
	}
	public String getFolderType() {
		
		return folderType;
	}
	
	public void setpageNum(int num) {
		
		pageNum=num;
	}
	public void setSlipId(String id) {
		
		slipId=id;
	}
	public void setFolderType(String type) {
		
		folderType=type;
	}
}
