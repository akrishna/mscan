/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.nevigation;

public final class R {
    public static final class anim {
        public static final int activity_in=0x7f040000;
        public static final int activity_out=0x7f040001;
        public static final int fly_in_from_center=0x7f040002;
    }
    public static final class array {
        public static final int country_codes=0x7f070000;
        public static final int imageResolution=0x7f070001;
        public static final int imageSize=0x7f070002;
        public static final int pref_example_list_titles=0x7f070003;
        public static final int pref_example_list_values=0x7f070004;
        public static final int pref_sync_frequency_titles=0x7f070005;
        public static final int pref_sync_frequency_values=0x7f070006;
    }
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarButtonStyle=0x7f010003;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarStyle=0x7f010002;
        /** <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
         */
        public static final int camera_id=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int show_fps=0x7f010000;
    }
    public static final class color {
        public static final int aqua=0x7f080021;
        public static final int background=0x7f080019;
        public static final int black=0x7f080027;
        public static final int black_overlay=0x7f080028;
        public static final int blue=0x7f080025;
        public static final int contents_text=0x7f080000;
        public static final int encode_view=0x7f080001;
        public static final int fuchsia=0x7f08001a;
        public static final int gray=0x7f08001d;
        public static final int green=0x7f080024;
        public static final int help_button_view=0x7f080002;
        public static final int help_view=0x7f080003;
        public static final int lime=0x7f080022;
        public static final int maroon=0x7f080020;
        public static final int navy=0x7f080026;
        public static final int olive=0x7f08001e;
        public static final int possible_result_points=0x7f080004;
        public static final int purple=0x7f08001f;
        public static final int red=0x7f08001b;
        public static final int result_image_border=0x7f080005;
        public static final int result_minor_text=0x7f080006;
        public static final int result_points=0x7f080007;
        public static final int result_text=0x7f080008;
        public static final int result_view=0x7f080009;
        public static final int sbc_header_text=0x7f08000a;
        public static final int sbc_header_view=0x7f08000b;
        public static final int sbc_layout_view=0x7f08000d;
        public static final int sbc_list_item=0x7f08000c;
        public static final int sbc_page_number_text=0x7f08000e;
        public static final int sbc_snippet_text=0x7f08000f;
        public static final int share_text=0x7f080010;
        public static final int silver=0x7f08001c;
        public static final int status_text=0x7f080011;
        public static final int teal=0x7f080023;
        public static final int trans=0x7f080018;
        public static final int transparent=0x7f080012;
        public static final int viewfinder_frame=0x7f080013;
        public static final int viewfinder_laser=0x7f080014;
        public static final int viewfinder_mask=0x7f080015;
        public static final int white=0x7f080016;
        public static final int yellow=0x7f080017;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f0b0000;
        public static final int activity_vertical_margin=0x7f0b0001;
    }
    public static final class drawable {
        public static final int button=0x7f020000;
        public static final int circle=0x7f020001;
        public static final int circleimage=0x7f020002;
        public static final int circleimage1=0x7f020003;
        public static final int circleimage2=0x7f020004;
        public static final int circleimage3=0x7f020005;
        public static final int circleimage4=0x7f020006;
        public static final int folder=0x7f020007;
        public static final int folder2=0x7f020008;
        public static final int food=0x7f020009;
        public static final int ic_action_camera=0x7f02000a;
        public static final int ic_action_discard=0x7f02000b;
        public static final int ic_action_edit=0x7f02000c;
        public static final int ic_action_new_attachment=0x7f02000d;
        public static final int ic_action_new_picture=0x7f02000e;
        public static final int ic_action_next_item=0x7f02000f;
        public static final int ic_action_overflow=0x7f020010;
        public static final int ic_action_picture=0x7f020011;
        public static final int ic_action_settings=0x7f020012;
        public static final int ic_launcher=0x7f020013;
        public static final int launcher_icon=0x7f020014;
        public static final int share_via_barcode=0x7f020015;
        public static final int shopper_icon=0x7f020016;
        public static final int zomato=0x7f020017;
    }
    public static final class id {
        public static final int action_settings=0x7f090067;
        public static final int addIcon=0x7f090025;
        public static final int allDelete=0x7f090044;
        public static final int angle=0x7f09001f;
        public static final int angleValue=0x7f090020;
        public static final int any=0x7f09000a;
        /**  Messages IDs 
         */
        public static final int auto_focus=0x7f090000;
        public static final int back=0x7f09000b;
        public static final int back_button=0x7f09003c;
        public static final int barcode_image_view=0x7f09002b;
        public static final int bookmark_title=0x7f090021;
        public static final int bookmark_url=0x7f090022;
        public static final int camera_holder=0x7f09000d;
        public static final int cancel=0x7f090014;
        public static final int capturePic=0x7f090056;
        public static final int capturedPic=0x7f090053;
        public static final int capturedPicNumber=0x7f090054;
        public static final int contents_supplement_text_view=0x7f090035;
        public static final int contents_text_view=0x7f090034;
        public static final int cornerImage1=0x7f090017;
        public static final int cornerImage2=0x7f090018;
        public static final int cornerImage3=0x7f090019;
        public static final int cornerImage4=0x7f09001a;
        public static final int currentSlip=0x7f09000f;
        public static final int currentSlipId=0x7f090010;
        public static final int decode=0x7f090001;
        public static final int decode_failed=0x7f090002;
        public static final int decode_succeeded=0x7f090003;
        public static final int defaultSavingLocation=0x7f09004e;
        public static final int detail=0x7f090049;
        public static final int details=0x7f09000e;
        public static final int discardIcon=0x7f090027;
        public static final int done_button=0x7f09003d;
        public static final int editIcon=0x7f090026;
        public static final int encode_view=0x7f090039;
        public static final int enterSlipId=0x7f090047;
        public static final int format_text_view=0x7f09002d;
        public static final int format_text_view_label=0x7f09002c;
        public static final int front=0x7f09000c;
        public static final int gridMenu=0x7f090024;
        public static final int gridPage=0x7f09001d;
        public static final int help_contents=0x7f09003b;
        public static final int history_detail=0x7f09003f;
        public static final int history_title=0x7f09003e;
        public static final int icon=0x7f090040;
        public static final int imageParent=0x7f090015;
        public static final int image_view=0x7f09003a;
        public static final int instruction=0x7f090013;
        public static final int ip=0x7f09004b;
        public static final int ipAddress=0x7f09004a;
        public static final int launch_product_query=0x7f090004;
        public static final int listTask=0x7f09005d;
        public static final int mainImageViewId=0x7f090016;
        public static final int menuName=0x7f090041;
        public static final int meta_text_view=0x7f090033;
        public static final int meta_text_view_label=0x7f090032;
        public static final int numPages=0x7f090011;
        public static final int numPagesValue=0x7f090012;
        public static final int numTask=0x7f09005c;
        public static final int num_menu=0x7f090023;
        public static final int num_page=0x7f09001c;
        public static final int pageName=0x7f090043;
        public static final int pageNum=0x7f090060;
        public static final int pageThumb=0x7f090042;
        public static final int page_number_view=0x7f09005a;
        public static final int pager=0x7f09001e;
        public static final int preview_view=0x7f090028;
        public static final int query_button=0x7f090058;
        public static final int query_text_view=0x7f090057;
        public static final int quit=0x7f090005;
        public static final int restart_preview=0x7f090006;
        public static final int result_button_view=0x7f090036;
        public static final int result_list_view=0x7f090059;
        public static final int result_view=0x7f09002a;
        public static final int return_scan_result=0x7f090007;
        public static final int savingLocation=0x7f09004d;
        public static final int scanLayout=0x7f090045;
        public static final int scanMenu=0x7f090055;
        public static final int scanSlipId=0x7f090046;
        public static final int scannedMenuPage=0x7f090051;
        public static final int scannedMenuPageNumber=0x7f090052;
        public static final int search_book_contents_failed=0x7f090008;
        public static final int search_book_contents_succeeded=0x7f090009;
        public static final int sessionIcon=0x7f09005f;
        public static final int sessionParent=0x7f09005e;
        public static final int share_app_button=0x7f090062;
        public static final int share_bookmark_button=0x7f090063;
        public static final int share_clipboard_button=0x7f090065;
        public static final int share_contact_button=0x7f090064;
        public static final int share_text_view=0x7f090066;
        public static final int shopper_button=0x7f090037;
        public static final int slipId=0x7f090061;
        public static final int slipIdTableRow=0x7f09004c;
        public static final int slipScanned=0x7f09004f;
        public static final int slipScannedNumber=0x7f090050;
        public static final int snippet_view=0x7f09005b;
        public static final int status_view=0x7f090038;
        public static final int submitSlipId=0x7f090048;
        public static final int time_text_view=0x7f090031;
        public static final int time_text_view_label=0x7f090030;
        public static final int transform=0x7f09001b;
        public static final int type_text_view=0x7f09002f;
        public static final int type_text_view_label=0x7f09002e;
        public static final int viewfinder_view=0x7f090029;
    }
    public static final class layout {
        public static final int activity_camera=0x7f030000;
        public static final int activity_image_display=0x7f030001;
        public static final int activity_list_page=0x7f030002;
        public static final int activity_main=0x7f030003;
        public static final int activity_splash=0x7f030004;
        public static final int angle_prompt=0x7f030005;
        public static final int bookmark_picker_list_item=0x7f030006;
        public static final int browse_frag=0x7f030007;
        public static final int capture=0x7f030008;
        public static final int encode=0x7f030009;
        public static final int help=0x7f03000a;
        public static final int history_list_item=0x7f03000b;
        public static final int menu_list_item=0x7f03000c;
        public static final int page_list_item=0x7f03000d;
        public static final int preference=0x7f03000e;
        public static final int preferences_layout=0x7f03000f;
        public static final int scan_frag=0x7f030010;
        public static final int search_book_contents=0x7f030011;
        public static final int search_book_contents_header=0x7f030012;
        public static final int search_book_contents_list_item=0x7f030013;
        public static final int session_frag=0x7f030014;
        public static final int session_list_item=0x7f030015;
        public static final int share=0x7f030016;
    }
    public static final class menu {
        public static final int browse_menu=0x7f0d0000;
        public static final int image_display=0x7f0d0001;
        public static final int list_page=0x7f0d0002;
        public static final int list_page_activity2=0x7f0d0003;
        public static final int main=0x7f0d0004;
        public static final int splash=0x7f0d0005;
    }
    public static final class raw {
        public static final int beep=0x7f060000;
    }
    public static final class string {
        public static final int action_settings=0x7f0a0078;
        public static final int app_name=0x7f0a0000;
        public static final int app_picker_name=0x7f0a0001;
        public static final int bookmark_picker_name=0x7f0a0002;
        public static final int button_add_calendar=0x7f0a0003;
        public static final int button_add_contact=0x7f0a0004;
        public static final int button_back=0x7f0a0005;
        public static final int button_book_search=0x7f0a0006;
        public static final int button_cancel=0x7f0a0007;
        public static final int button_clipboard_empty=0x7f0a0008;
        public static final int button_custom_product_search=0x7f0a0009;
        public static final int button_dial=0x7f0a000a;
        public static final int button_done=0x7f0a000b;
        public static final int button_email=0x7f0a000c;
        public static final int button_get_directions=0x7f0a000d;
        public static final int button_google_shopper=0x7f0a000e;
        public static final int button_mms=0x7f0a000f;
        public static final int button_ok=0x7f0a0010;
        public static final int button_open_browser=0x7f0a0011;
        public static final int button_product_search=0x7f0a0012;
        public static final int button_search_book_contents=0x7f0a0013;
        public static final int button_share_app=0x7f0a0014;
        public static final int button_share_bookmark=0x7f0a0015;
        public static final int button_share_by_email=0x7f0a0016;
        public static final int button_share_by_sms=0x7f0a0017;
        public static final int button_share_clipboard=0x7f0a0018;
        public static final int button_share_contact=0x7f0a0019;
        public static final int button_show_map=0x7f0a001a;
        public static final int button_sms=0x7f0a001b;
        public static final int button_web_search=0x7f0a001c;
        public static final int button_wifi=0x7f0a001d;
        public static final int contents_contact=0x7f0a001e;
        public static final int contents_email=0x7f0a001f;
        public static final int contents_location=0x7f0a0020;
        public static final int contents_phone=0x7f0a0021;
        public static final int contents_sms=0x7f0a0022;
        public static final int contents_text=0x7f0a0023;
        public static final int dummy_button=0x7f0a007e;
        public static final int dummy_content=0x7f0a007f;
        public static final int hello_world=0x7f0a0079;
        public static final int history_clear_one_history_text=0x7f0a0025;
        public static final int history_clear_text=0x7f0a0024;
        public static final int history_email_title=0x7f0a0026;
        public static final int history_empty=0x7f0a0027;
        public static final int history_empty_detail=0x7f0a0028;
        public static final int history_send=0x7f0a0029;
        public static final int history_title=0x7f0a002a;
        public static final int menu_about=0x7f0a002b;
        public static final int menu_encode_mecard=0x7f0a002c;
        public static final int menu_encode_vcard=0x7f0a002d;
        public static final int menu_help=0x7f0a002e;
        public static final int menu_history=0x7f0a002f;
        public static final int menu_settings=0x7f0a0030;
        public static final int menu_share=0x7f0a0031;
        public static final int msg_about=0x7f0a0032;
        public static final int msg_bulk_mode_scanned=0x7f0a0033;
        public static final int msg_camera_framework_bug=0x7f0a0034;
        public static final int msg_default_format=0x7f0a0035;
        public static final int msg_default_meta=0x7f0a0036;
        public static final int msg_default_mms_subject=0x7f0a0037;
        public static final int msg_default_status=0x7f0a0038;
        public static final int msg_default_time=0x7f0a0039;
        public static final int msg_default_type=0x7f0a003a;
        public static final int msg_encode_contents_failed=0x7f0a003b;
        public static final int msg_google_books=0x7f0a003c;
        public static final int msg_google_product=0x7f0a003d;
        public static final int msg_google_shopper_missing=0x7f0a003e;
        public static final int msg_install_google_shopper=0x7f0a003f;
        public static final int msg_intent_failed=0x7f0a0040;
        public static final int msg_redirect=0x7f0a0041;
        public static final int msg_sbc_book_not_searchable=0x7f0a0042;
        public static final int msg_sbc_failed=0x7f0a0043;
        public static final int msg_sbc_no_page_returned=0x7f0a0044;
        public static final int msg_sbc_page=0x7f0a0045;
        public static final int msg_sbc_searching_book=0x7f0a0046;
        public static final int msg_sbc_snippet_unavailable=0x7f0a0047;
        public static final int msg_sbc_unknown_page=0x7f0a0048;
        public static final int msg_share_explanation=0x7f0a0049;
        public static final int msg_share_subject_line=0x7f0a004a;
        public static final int msg_share_text=0x7f0a004b;
        public static final int msg_sure=0x7f0a004c;
        public static final int msg_unmount_usb=0x7f0a004d;
        public static final int pref_default_display_name=0x7f0a0085;
        public static final int pref_description_social_recommendations=0x7f0a0083;
        /**  Example settings for Data & Sync 
         */
        public static final int pref_header_data_sync=0x7f0a0087;
        /**  Strings related to Settings 
 Example General settings 
         */
        public static final int pref_header_general=0x7f0a0081;
        /**  Example settings for Notifications 
         */
        public static final int pref_header_notifications=0x7f0a008a;
        public static final int pref_ringtone_silent=0x7f0a008d;
        public static final int pref_title_add_friends_to_messages=0x7f0a0086;
        public static final int pref_title_display_name=0x7f0a0084;
        public static final int pref_title_new_message_notifications=0x7f0a008b;
        public static final int pref_title_ringtone=0x7f0a008c;
        public static final int pref_title_social_recommendations=0x7f0a0082;
        public static final int pref_title_sync_frequency=0x7f0a0088;
        public static final int pref_title_system_sync_settings=0x7f0a0089;
        public static final int pref_title_vibrate=0x7f0a008e;
        public static final int preferences_actions_title=0x7f0a004e;
        public static final int preferences_bulk_mode_summary=0x7f0a004f;
        public static final int preferences_bulk_mode_title=0x7f0a0050;
        public static final int preferences_copy_to_clipboard_title=0x7f0a0051;
        public static final int preferences_custom_product_search_summary=0x7f0a0052;
        public static final int preferences_custom_product_search_title=0x7f0a0053;
        public static final int preferences_decode_1D_title=0x7f0a0054;
        public static final int preferences_decode_Data_Matrix_title=0x7f0a0055;
        public static final int preferences_decode_QR_title=0x7f0a0056;
        public static final int preferences_front_light_summary=0x7f0a0057;
        public static final int preferences_front_light_title=0x7f0a0058;
        public static final int preferences_general_title=0x7f0a0059;
        public static final int preferences_name=0x7f0a005a;
        public static final int preferences_play_beep_title=0x7f0a005b;
        public static final int preferences_remember_duplicates_summary=0x7f0a005c;
        public static final int preferences_remember_duplicates_title=0x7f0a005d;
        public static final int preferences_result_title=0x7f0a0060;
        public static final int preferences_reverse_image_summary=0x7f0a005e;
        public static final int preferences_reverse_image_title=0x7f0a005f;
        public static final int preferences_scanning_title=0x7f0a0061;
        public static final int preferences_search_country=0x7f0a0062;
        public static final int preferences_supplemental_summary=0x7f0a0063;
        public static final int preferences_supplemental_title=0x7f0a0064;
        public static final int preferences_vibrate_title=0x7f0a0065;
        public static final int result_address_book=0x7f0a0066;
        public static final int result_calendar=0x7f0a0067;
        public static final int result_email_address=0x7f0a0068;
        public static final int result_geo=0x7f0a0069;
        public static final int result_isbn=0x7f0a006a;
        public static final int result_product=0x7f0a006b;
        public static final int result_sms=0x7f0a006c;
        public static final int result_tel=0x7f0a006d;
        public static final int result_text=0x7f0a006e;
        public static final int result_uri=0x7f0a006f;
        public static final int result_wifi=0x7f0a0070;
        public static final int sbc_name=0x7f0a0071;
        public static final int share_name=0x7f0a0072;
        public static final int title_about=0x7f0a0073;
        public static final int title_activity_image_display=0x7f0a007b;
        public static final int title_activity_list_page=0x7f0a007a;
        public static final int title_activity_list_page_activity2=0x7f0a007c;
        public static final int title_activity_settings=0x7f0a0080;
        public static final int title_activity_splash=0x7f0a007d;
        public static final int wifi_changing_network=0x7f0a0074;
        public static final int wifi_ssid_label=0x7f0a0075;
        public static final int wifi_type_label=0x7f0a0076;
        public static final int zxing_url=0x7f0a0077;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f0c0000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f0c0001;
        public static final int ButtonBar=0x7f0c0003;
        public static final int ButtonBarButton=0x7f0c0004;
        public static final int FullscreenActionBarStyle=0x7f0c0005;
        public static final int FullscreenTheme=0x7f0c0002;
    }
    public static final class xml {
        public static final int pref_data_sync=0x7f050000;
        public static final int pref_general=0x7f050001;
        public static final int pref_headers=0x7f050002;
        public static final int pref_notification=0x7f050003;
        public static final int preferences=0x7f050004;
    }
    public static final class styleable {
        /** 
         Declare custom theme attributes that allow changing which styles are
         used for button bars depending on the API level.
         ?android:attr/buttonBarStyle is new as of API 11 so this is
         necessary to support previous API levels.
    
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarButtonStyle com.example.nevigation:buttonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarStyle com.example.nevigation:buttonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_buttonBarButtonStyle
           @see #ButtonBarContainerTheme_buttonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010002, 0x7f010003
        };
        /**
          <p>This symbol is the offset where the {@link com.example.nevigation.R.attr#buttonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.nevigation:buttonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.nevigation.R.attr#buttonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.nevigation:buttonBarStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
        /** Attributes that can be used with a CameraBridgeViewBase.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_camera_id com.example.nevigation:camera_id}</code></td><td></td></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_show_fps com.example.nevigation:show_fps}</code></td><td></td></tr>
           </table>
           @see #CameraBridgeViewBase_camera_id
           @see #CameraBridgeViewBase_show_fps
         */
        public static final int[] CameraBridgeViewBase = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.example.nevigation.R.attr#camera_id}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
          @attr name com.example.nevigation:camera_id
        */
        public static final int CameraBridgeViewBase_camera_id = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.nevigation.R.attr#show_fps}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.nevigation:show_fps
        */
        public static final int CameraBridgeViewBase_show_fps = 0;
    };
}
